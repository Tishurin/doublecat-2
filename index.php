<?php

$webRoot = dirname(__FILE__);

if($_SERVER['HTTP_HOST'] == 'dw-doublecat-2'){
    define('YII_DEBUG', true);
    require_once('/Applications/MAMP/bin/Yii/yii-1.1.14-2/framework/yii.php');
    $configFile = $webRoot . '/protected/config/config_local.php';
}
else { 
    define('YII_DEBUG', false);
    require_once('/Applications/MAMP/bin/Yii/yii-1.1.14-2/framework/yii.php');
    $configFile = $webRoot . '/protected/config/config_server.php';
}



Yii::createWebApplication($configFile)->run();

<?php 
	# ver: 2.0.0 
?>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />


	<?php $cs = Yii::app()->clientScript ?>
	<?php if ($this->pageDescription != '') $cs->registerMetaTag($this->pageDescription, 'description') ?>
	<?php if ($this->pageKeywords != '') $cs->registerMetaTag($this->pageKeywords, 'keywords') ?>



	<title><?php echo CHtml::encode($this->pageTitle  . ' | ' . Yii::app()->name); ?></title>
</head>
<body>

	<div style='margin: 0px auto; width: 1024px; vertical-align: middle; text-align: center; border: 1px solid black;'>
		<?php echo Yii::app()->name; ?>		
	</div>

	<div style='margin: 10px auto; width: 1024px;'>
		
		<?php if ($this->showBreadcrumbs): ?>
			<?php $this->widget('bootstrap.widgets.TbBreadcrumb', array(
				'homeLabel' => 'Главная',
			    'links' => $this->breadcrumbs + array($this->getPageBreadcrumb())
			)); ?>
		<?php endif ?>

		<?php if ($this->showHeader): ?>
			<h1><?php echo Yii::app()->controller->getPageHeader() ?></h1>					
		<?php endif ?>	
		
		<?php echo $content ?>

	</div>



</body>
</html>
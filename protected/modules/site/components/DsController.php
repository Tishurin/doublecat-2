<?php

# ver: 2.0.0

class DsController extends DController
{
	public $layout = '/layout/site';
	public $errorView = '/site/error';

	// СЕО - примочки
	public $pageDescription;
	public $pageKeywords;
	private $_pageHeader;
	private $_pageBreadcrumb;

	//----------------------------------------------------------------------------
	public function getPageHeader()
	//----------------------------------------------------------------------------
	{
		if ($this->_pageHeader === null)
			return $this->_pageHeader = $this->getPageTitle();

		else return $this->_pageHeader;
	}

	//----------------------------------------------------------------------------
	public function setPageHeader($value)
	//----------------------------------------------------------------------------
	{
		$this->_pageHeader = $value;
	}

	//----------------------------------------------------------------------------
	public function getPageBreadcrumb()
	//----------------------------------------------------------------------------
	{
		if ($this->_pageBreadcrumb === null)
			return $this->_pageBreadcrumb = $this->getPageHeader();

		else return $this->_pageBreadcrumb;
	}


	//----------------------------------------------------------------------------
	public function setPageBreadcrumb($value)
	//----------------------------------------------------------------------------
	{
		$this->_pageBreadcrumb = $value;
	}

}
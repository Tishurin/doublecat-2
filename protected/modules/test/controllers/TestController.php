<?php

class TestController extends DcController
{
	public $defaultAction = 'admin';

	public $titles = array(
		'admin' => 'Администрирование',
		'update' => 'Редактировать',
		'create' => 'Добавить',
		'view' => 'Просмотр',	// Меняется в loadModel
		'list' => 'Список',	
		);

	//----------------------------------------------------------------------------
	public function actions()
	//----------------------------------------------------------------------------
	{
		return array(
			'sort' => array(
				'class' => 'DSortAction',
			),
			'upload' => array(
				'class' => 'files.components.DUploadAction',
				'thumb' => 'html',
			),

		);
	}	


	//----------------------------------------------------------------------------
	public function actionList()
	//----------------------------------------------------------------------------
	{
		$modTest = new Test('search');
		$modTest->unsetAttributes();
		
		if(isset($_GET['Test'])) $modTest->attributes = $_GET['Test'];

		$this->menu = array(
			array('label' => 'Опции'), 

			array('label' => $this->getTitle('create'), 
					'icon' => 'plus',
					'url' => array($r = 'create'), 
					'visible' => $this->checkAccessOnce($r),
			),
		);

		$this->render('list', array('modTest' => $modTest));
	}

	//----------------------------------------------------------------------------
	public function actionAdmin()
	//----------------------------------------------------------------------------
	{
		$modTest = new Test('search');
		$modTest->unsetAttributes();
		
		if(isset($_GET['Test'])) $modTest->attributes = $_GET['Test'];

		$this->menu = array(                          
			array('label' => 'Опции'), 

			array('label' => $this->getTitle('create'), 
					'icon' => 'plus',
					'url' => array($r = 'create'), 
					'visible' => $this->checkAccessOnce($r),
			),
		);


		$this->render('admin', array('modTest' => $modTest));
	}

	//----------------------------------------------------------------------------
	public function actionView($id)
	//----------------------------------------------------------------------------
	{
		$modTest = $this->loadModel($id);
		
		$this->menu = array(
			array('label' => 'Опции'), 
			
			array('label'=>'Редактировать', 
						'icon' => 'pencil',
						'url' => array($r = 'update', 'id' => $modTest->id), 
						'visible' => $this->checkAccessOnce($r),
			),
			
			array('label'=>'Удалить', 
						'icon' => 'trash',
						'url' => '#', 
						'linkOptions'=>array('submit'=>array($r = 'delete', 'id' => $modTest->id), 'confirm'=>'Точно?'), 
						'visible' => $this->checkAccessOnce($r),
			),
		);

		$this->addBreadcrumb('admin');
		
		$this->render('view', array('modTest' => $modTest));
	}

	//----------------------------------------------------------------------------
	public function actionCreate()
	//----------------------------------------------------------------------------
	{
		$modTest = new Test;

		if(isset($_POST['Test']))
		{
			$modTest->attributes = $_POST['Test'];
			if($modTest->save())
				$this->redirectPageStatePrevious();
		}
		else $this->initPageStatePrevious();

		$this->addBreadcrumb('admin');

		$this->render('create', array('modTest' => $modTest));
	}

	//----------------------------------------------------------------------------
	public function actionUpdate($id)
	//----------------------------------------------------------------------------
	{
		$modTest = $this->loadModel($id);

		if(isset($_POST['Test']))
		{
			$modTest->attributes = $_POST['Test'];
			if($modTest->save())
				$this->redirectPageStatePrevious();
		}
		else $this->initPageStatePrevious(array('view', 'id' => $modTest->id)); // Переопределите если нужно

		$this->addViewBreadcrumb($modTest);

		$this->render('update', array('modTest' => $modTest));
	}

	//----------------------------------------------------------------------------
	public function actionDelete($id)
	//----------------------------------------------------------------------------  
	{
		if(Yii::app()->request->isPostRequest)
		{
			// we only allow deletion via POST request
			$this->loadModel($id)->delete();

			// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
			if(!isset($_GET['ajax']))
				$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
		}
		else
			throw new CHttpException(400,'Ошибка запроса.');
	}

	//----------------------------------------------------------------------------
	public function addViewBreadcrumb($modTest)
	//----------------------------------------------------------------------------
	{
		$this->addBreadcrumb('admin');
		$this->addBreadcrumb('view', array('view', 'id' => $modTest->id));
	}

	//----------------------------------------------------------------------------
	public function loadModel($id)
	//----------------------------------------------------------------------------
	{
		$modTest = Test::model()->findByPk($id);
		if($modTest === null)
			throw new CHttpException(404,'Такой страницы нет!');

		$this->titles['view'] = $this->titles['view'] . ' № ' . $modTest->id; 	
		return $modTest;
	}
}

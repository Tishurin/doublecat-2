
<?php $form=$this->beginWidget('CActiveForm', array(
	'action' => Yii::app()->createUrl($this->route),
	'method' => 'get',
	'htmlOptions' => array('style' => 'margin-bottom: 0;'),
)); ?>
	<div class='well well-small' style='padding-right: 130px; margin-bottom: 15px;'>
		<?php echo TbHtml::submitButton('Искать', array('color' => TbHtml::BUTTON_COLOR_PRIMARY, 'type' => TbHtml::BUTTON_TYPE_SUBMIT, 'icon' => 'icon-search icon-white', 'style' => 'float: right; text-align: left; margin-right: -120px; width: 100px;')); ?> 
		<?php echo $form->textField($modTest, 'request', array('style' => 'margin-bottom: 0; width: 100%;', 'placeholder' => 'Введите поисковое слово')); ?> 
	</div>
<?php $this->endWidget(); ?>

<?php $this->widget('bootstrap.widgets.TbGridView', array(
	'id' => 'test-grid',
	'dataProvider' => $modTest->search(),
	'type'=>'striped bordered',
	'template' => '{items}{pager}',
	'selectableRows' => 0,
	'emptyText' => 'Ничего не нашлось...',
	'filter' => $modTest,
	'ajaxUpdate' => false,
	'columns'=>array(
		array('name' => 'id', 'htmlOptions' => array()),
		array('name' => 'name', 'htmlOptions' => array()),
		array('name' => 'text', 'htmlOptions' => array()),
		array('name' => 'id_file', 'htmlOptions' => array()),
		array('name' => 'id_image', 'htmlOptions' => array()),
		array('name' => 'id_sort', 'htmlOptions' => array()),

		array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
			'template' => '{moveUp} {moveDown} {view} {update} {delete}',
			'htmlOptions' => array('style' => 'width: 90px;'),
			'buttons' => array(
				'moveUp' => array(
					'icon' => 'arrow-up',
					'url' => 'Yii::app()->controller->createUrl("sort", array("id" => $data->id))',
					'label' => 'Передвинуть вверх',
					),
				'moveDown' => array(
					'icon' => 'arrow-down',
					'url' => 'Yii::app()->controller->createUrl("sort", array("sort", "id" => $data->id, "d" => 1))',
					'label' => 'Передвинуть вниз',					
					),	
			),
		),
	),
)); ?>

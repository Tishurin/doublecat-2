
<?php $form = $this->beginWidget('DActiveForm', array(
	'id'=>'test-form',
	'stateful' => true,
	'htmlOptions' => array('class' => 'form-horizontal',
												 'enctype' => 'multipart/form-data'),
	'enableAjaxValidation' => false,
	'enableClientValidation' => false,
)); ?>

	<fieldset>

		<div class="control-group">
			<div class='controls'>          
				<?php if(count($modTest->errors) > 0): ?>
					<div class="span12 alert alert-error">
					 <?php echo $form->errorSummary($modTest); ?>
					</div>
				<?php endif ?>
			</div>
		</div>
		
		<!-- NAME -->
		<div class="control-group <?php if (isset($modTest->errors['name'])) echo 'error'; ?> ">
			<?php echo $form->labelEx($modTest, 'name', array('class' => 'control-label')); ?>
			<div class='controls'>
				<?php echo $form->textField($modTest, 'name', array('class' => 'span12')); ?>
				<?php echo $form->error($modTest, 'name', array('class' => 'help-block')); ?>
			</div>
		</div>
		
		<!-- TEXT -->
		<div class="control-group <?php if (isset($modTest->errors['text'])) echo 'error'; ?> ">
			<?php echo $form->labelEx($modTest, 'text', array('class' => 'control-label')); ?>
			<div class='controls'>
				<?php echo $form->htmlArea($modTest, 'text', array('class' => 'span12', 'style' => 'height: 300px;', 'options' => array('imageUploadCallback' => 'function(image, json){f = $("#test"); f.val(f.val() + " " + json.fileId);}'))); ?>
				<?php echo $form->error($modTest, 'text', array('class' => 'help-block')); ?>
			</div>
		</div>
		<textarea id="test"></textarea>
		<!-- ID_FILE -->
		<div class="control-group <?php if (isset($modTest->errors['id_file'])) echo 'error'; ?> ">
			<?php echo $form->labelEx($modTest, 'id_file', array('class' => 'control-label')); ?>
			<div class='controls'>
				<?php echo $form->textField($modTest, 'id_file', array('class' => 'span12')); ?>
				<?php echo $form->error($modTest, 'id_file', array('class' => 'help-block')); ?>
			</div>
		</div>
		
		<!-- ID_IMAGE -->
		<div class="control-group <?php if (isset($modTest->errors['id_image'])) echo 'error'; ?> ">
			<?php echo $form->labelEx($modTest, 'id_image', array('class' => 'control-label')); ?>
			<div class='controls'>
				<?php echo $form->textField($modTest, 'id_image', array('class' => 'span12')); ?>
				<?php echo $form->error($modTest, 'id_image', array('class' => 'help-block')); ?>
			</div>
		</div>
		
		<!-- ID_SORT -->
		<div class="control-group <?php if (isset($modTest->errors['id_sort'])) echo 'error'; ?> ">
			<?php echo $form->labelEx($modTest, 'id_sort', array('class' => 'control-label')); ?>
			<div class='controls'>
				<?php echo $form->textField($modTest, 'id_sort', array('class' => 'span12')); ?>
				<?php echo $form->error($modTest, 'id_sort', array('class' => 'help-block')); ?>
			</div>
		</div>
		
	</fieldset>

	<div class="form-actions">
		<?php echo TbHtml::linkButton('Отмена', array('color' => TbHtml::BUTTON_COLOR_DANGER, 'url' => $this->pageStatePrevious, 'icon' => 'remove white')); ?> 
		<?php echo TbHtml::submitButton('Готово', array('color' => TbHtml::BUTTON_COLOR_PRIMARY, 'icon' => 'ok white')); ?> 
	</div>


<?php $this->endWidget(); ?>

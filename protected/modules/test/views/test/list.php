
<?php $form=$this->beginWidget('CActiveForm', array(
	'action' => Yii::app()->createUrl($this->route),
	'method' => 'get',
	'htmlOptions' => array('style' => 'margin-bottom: 0;'),
)); ?>
	<div class='well well-small' style='padding-right: 130px; margin-bottom: 0;'>
		<?php echo TbHtml::submitButton('Искать', array('color' => TbHtml::BUTTON_COLOR_PRIMARY, 'type' => TbHtml::BUTTON_TYPE_SUBMIT, 'icon' => 'icon-search icon-white', 'style' => 'float: right; text-align: left; margin-right: -120px; width: 100px;')); ?> 
		<?php echo $form->textField($modTest, 'request', array('style' => 'margin-bottom: 0; width: 100%;')); ?> 
	</div>
<?php $this->endWidget(); ?>


<?php $this->widget('bootstrap.widgets.TbListView', array(
	'dataProvider' => $modTest->search(),
  'template' => '{pager}{items}{pager}',
	'itemView' => 'listItem',
)); ?>

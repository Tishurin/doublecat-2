<?php

//******************************************************************************
class Test extends DActiveRecord
//******************************************************************************
{
	public $request; // Поисковый запрос

	//****************************************************************************
	// AR - методы
	//****************************************************************************

	//----------------------------------------------------------------------------
	public function attributeLabels()
	//----------------------------------------------------------------------------  
	{
		return array(
			'id' => 'ID',
			'name' => 'Имя',
			'text' => 'Текст',
			'id_file' => 'Id File',
			'id_image' => 'Id Image',
			'id_sort' => 'Id Sort',
		);
	}

	//----------------------------------------------------------------------------
	public function rules()
	//----------------------------------------------------------------------------	
	{
		return array(
				array('id_file, id_image, id_sort', 'required'),
				array('id_file, id_image, id_sort', 'numerical', 'integerOnly'=>true),
				array('name', 'length', 'max'=>255),
				array('text', 'safe'),
	
				// Внимание: удалите лишние атрибуты!
				array('id, name, text, id_file, id_image, id_sort, request', 'safe', 'on'=>'search'),
		);
	}

	//----------------------------------------------------------------------------
	public function relations()
	//----------------------------------------------------------------------------
	{
		// ВНИМАНИЕ: уточните имя связи
		return array(
		);
	}

	//----------------------------------------------------------------------------
	public function behaviors()
	//----------------------------------------------------------------------------
	{
		return array(
			'sort' => array(
				'class' => 'DSortBehavior',
				'sort' => 'id_sort',		
				// 'parent' => 'id_parent'
			),
		);
		

	}

	//****************************************************************************
	// Пользовательские методы
	//****************************************************************************


	//****************************************************************************
	// Поиск
	//****************************************************************************

	//----------------------------------------------------------------------------
	public function search()
	//----------------------------------------------------------------------------
	{
		// Внимание: удалите лишние атрибуты!
		$objCriteria = new CDbCriteria;

		// Фильтрация полей
		$objCriteria->compare('id', $this->id, true);
		$objCriteria->compare('name', $this->name, true);
		$objCriteria->compare('text', $this->text, true);
		$objCriteria->compare('id_file', $this->id_file);
		$objCriteria->compare('id_image', $this->id_image);
		$objCriteria->compare('id_sort', $this->id_sort);

		// Запрос по строке
		$objCriteria->compare('id', $this->request, false, 'OR');
		$objCriteria->compare('name', $this->request, false, 'OR');
		$objCriteria->compare('text', $this->request, false, 'OR');
		$objCriteria->compare('id_file', $this->request, false, 'OR');
		$objCriteria->compare('id_image', $this->request, false, 'OR');
		$objCriteria->compare('id_sort', $this->request, false, 'OR');


		$objCriteria->order = 'id_sort ASC';


		return new CActiveDataProvider($this, array(
			'criteria' => $objCriteria,
			'sort' => false,
			'pagination' => array('pageSize' => 25),
		));
	}


	public static function model($className=__CLASS__) {return parent::model($className);}
	public function tableName() {return 'tbl_test';}
}
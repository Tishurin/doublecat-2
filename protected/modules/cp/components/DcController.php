<?php

# ver: 2.0.0

class DcController extends DController
{
	public $layout = '//cp/CpLayout';
	public $errorAction = '/cp/cp/error';
	public $errorView = '//cp/error';


	//----------------------------------------------------------------------------
	public function init()
	//----------------------------------------------------------------------------
	{
		parent::init();

		Yii::app()->errorHandler->errorAction = $this->errorAction;
	}	
}
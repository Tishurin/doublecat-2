<?php

# ver: 2.0.0

class CpController extends DcController
{
	public $titles = array(
		'index' => 'Главная',	
		);

	//----------------------------------------------------------------------------
	public function actionIndex()
	//----------------------------------------------------------------------------
	{
		$this->showBreadcrumbs = false;

		$this->render('index');
	}
}


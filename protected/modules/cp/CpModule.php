<?php

# ver: 2.0.0

class CpModule extends CWebModule
{
	public $defaultController = '/cp/index';

	//----------------------------------------------------------------------------
	public function init()
	//----------------------------------------------------------------------------
	{
		// import the module-level models and components
		$this->setImport(array(
			'cp.models.*',
			'cp.components.*',
		));
	}

	//----------------------------------------------------------------------------
	public function beforeControllerAction($controller, $action)
	//----------------------------------------------------------------------------
	{
		if(parent::beforeControllerAction($controller, $action))
		{
			// this method is called before any module controller action is performed
			// you may place customized code here
			return true;
		}
		else
			return false;
	}
}

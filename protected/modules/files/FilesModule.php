<?php

# ver: 2.0.0.1
# Назначение прав доступа созданным файлам

class FilesModule extends CWebModule
{
	public $uploadPath;
	public $assetsPath;
	public $assetsUrl;

	public $noimageUrl;

	public $debug = false;

	public $maxImageSize = 1048576;

	public $thumbs;

	//----------------------------------------------------------------------------	 
	public function init()
	//----------------------------------------------------------------------------	 
	{
		$this->uploadPath = Yii::getPathOfAlias('application.data') . '/files';
		$this->assetsPath = Yii::getPathOfAlias('application') . '/../assets/files';
		$this->assetsUrl = Yii::app()->baseUrl . '/assets/files';

		if (!is_dir($this->assetsPath)) {
			mkdir($this->assetsPath);
			chmod($this->assetsPath, 0777);
		}

		$this->setImport(array(
			'files.models.*',
			'files.components.*',
		));
	}

	//----------------------------------------------------------------------------	 
	public function beforeControllerAction($controller, $action)
	//----------------------------------------------------------------------------	 
	{
		if(parent::beforeControllerAction($controller, $action))
		{
			// this method is called before any module controller action is performed
			// you may place customized code here
			return true;
		}
		else
			return false;
	}
}

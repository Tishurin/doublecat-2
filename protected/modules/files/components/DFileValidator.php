<?php

# ver: 2.0.0

//------------------------------------------------------------------------------
class DFileValidator extends CFileValidator
//------------------------------------------------------------------------------
{
	//----------------------------------------------------------------------------
	public function validate($object, $attributes)
	//----------------------------------------------------------------------------
	{
		$object->attachEventHandler('onBeforeSave', array($this, 'beforeSave'));
		return parent::validate($object, $attributes);
	}

	//----------------------------------------------------------------------------
	protected function validateAttribute($object, $attribute)
	//----------------------------------------------------------------------------
	{
		if ($objFile = CUploadedFile::getInstanceByName(CHtml::activeName($object, $attribute . '_file'))) 
			$object->$attribute = $objFile;
					
		parent::validateAttribute($object, $attribute);
	}  

	//----------------------------------------------------------------------------
	protected function emptyAttribute($object, $attribute)
	//----------------------------------------------------------------------------
	{
		if ($object->$attribute == null) 
			parent::emptyAttribute($object, $attribute);
	}

	//----------------------------------------------------------------------------
	public function beforeSave($event)
	//----------------------------------------------------------------------------
	{

		foreach ($event->sender->attributes as $key => $attribute) {
			if ($attribute instanceof CUploadedFile) {

				$modFile = File::createByFile($attribute);
				
				// Выполнить сохранение и регистрацию загруженного файла
				if ($modFile->save()) $event->sender->$key = $modFile->id;
				else {
					foreach ($modFile->errors as $arrErrors)
						foreach ($arrErrors as $strError)
							$event->sender->addError($key, $strError);

					$event->isValid = false;
				} 
			}
		}
	}
}

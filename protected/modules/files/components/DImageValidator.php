<?php

# ver: 2.0.0

//------------------------------------------------------------------------------
class DImageValidator extends DFileValidator
//------------------------------------------------------------------------------
{
	//----------------------------------------------------------------------------
	protected function validateAttribute($object, $attribute)
	//----------------------------------------------------------------------------
	{
		parent::validateAttribute($object, $attribute);

		// И отдельно пройтись по картинке
		if ($object->$attribute instanceof CUploadedFile) 
			$this->validateImage($object, $attribute);
	}  


	//----------------------------------------------------------------------------
	public function validateImage($object, $attribute)
	//----------------------------------------------------------------------------
	{
		$imagePath = $object->$attribute->tempName;

		// Вначале проверить существование
		if (!is_file($imagePath))
			$arrErrors[] = 'Файл не найден';

		// Проверить тип
		if (!($t = getimagesize($imagePath))) $object->addError($attribute, 'Загруженный файл не является изображением');
		elseif (!in_array($t[2], array(IMAGETYPE_GIF, IMAGETYPE_JPEG, IMAGETYPE_BMP, IMAGETYPE_PNG))) 
			$object->addError($attribute, 'Загруженный файл имеет неверный тип. Поддерживаются типы JPG, GIF, PNG, BMP');

		// Проверить размер
		$maxSize = Yii::app()->getModule("files")->maxImageSize;

		if ($this->maxSize !== null)
			$maxSize = min($maxSize, $this->maxSize);

		if (@filesize($imagePath) > $maxSize)
			$object->addError($attribute, 'Картинка слишком большая... Она должна быть не больше ' . Yii::app()->format->size($maxSize));
	}

}

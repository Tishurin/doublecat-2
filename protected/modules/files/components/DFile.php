<?php

# ver: 2.0.0.1
# Назначение прав после создания

//******************************************************************************
class DFile extends DActiveRecord
//******************************************************************************
{
	//****************************************************************************
	// AR - методы
	//****************************************************************************

	//----------------------------------------------------------------------------
	public function beforeDelete()
	//----------------------------------------------------------------------------
	// Удаляет сам файл
	{
		if (!parent::beforeDelete()) return false;

		if (is_file($this->sourcePath))
			unlink($this->sourcePath);
		
		return true;
	}

	//----------------------------------------------------------------------------
	public function beforeSave()
	//----------------------------------------------------------------------------
	// Если источник - CUploadedFile, то сохранить его стандартным способом
	{
		if (!parent::beforeSave()) return false;

		// Загрузка файла без валидатора
		if ($this->source instanceof CUploadedFile)
		{
			$source = uniqid();
			$uploadPath = $this->module->uploadPath;

			// Создать подпапку
			if (!is_dir($uploadPath . '/' . $source[4]))
				mkdir($uploadPath . '/' . $source[4], 0777);

			$source = $source[4] . '/' . $source;
			$this->source->saveAs($uploadPath . '/' .  $source);
			$this->source = $source;
			$this->tst_upload = time();
		}

		return true;
	}

	//****************************************************************************
	// HTML - методы
	//****************************************************************************

	//----------------------------------------------------------------------------
	public static function activeFileField($model, $attribute, $htmlOptions = array())
	//----------------------------------------------------------------------------
	{
		$attr_name = CHtml::activeName($model, $attribute);
		$file_name = substr($attr_name, 0, -1) . '_file]';
		
		// Отобразить загруженную картинку
		/*
		$modFile = DFile::model()->findByPk($model->$attribute);
		if ($modFile === null) $strFile = 'Файл не загружен';
		else $strFile = $modFile->downloadLink();
		*/

		return (CHtml::fileField($file_name));
	}

	//----------------------------------------------------------------------------
	public static function activeImageField($model, $attribute, $htmlOptions = array())
	//----------------------------------------------------------------------------  
	// Картинка
	{
		$attr_name = CHtml::activeName($model, $attribute);
		$file_name = substr($attr_name, 0, -1) . '_file]';
		
		// Отобразить загруженную картинку
		$modImage = File::model()->findByPk($model->$attribute);

		$strImage = '';
		if ($modImage != null) 
			$strImage = self::image($modImage, array('width' => 50, 'height' => 50, 'mode' => THUMB_MODE_CROP), '', array('style' => 'margin-left: -60px; float: left;'));

		return '<div class="well span12" style="padding: 10px 10px 10px 70px; margin-bottom: 0;">' . $strImage . CHtml::fileField($file_name) . '<div style="clear:both;"></div></div>';
	}  

	//----------------------------------------------------------------------------
	public static function image($modFile, $thumb, $alt = '', $htmlOptions = array())
	//----------------------------------------------------------------------------
	{
		$sttThumb = self::normalizeThumb($thumb);

		if (!isset($htmlOptions['width']) && $sttThumb['width'] != 0)
			$htmlOptions['width'] = $sttThumb['width'] . 'px';

		if (!isset($htmlOptions['height']) && $sttThumb['height'] != 0)
			$htmlOptions['height'] = $sttThumb['height'] . 'px';

		if ($modFile == null || !($src = $modFile->publish($sttThumb)))
			$src = $sttThumb['noimageUrl'];
			
		return CHtml::image($src, $alt, $htmlOptions);	
	}

	//****************************************************************************
	// Публикация миниатюр
	//****************************************************************************

	//----------------------------------------------------------------------------
	public function publish($varThumb)
	//----------------------------------------------------------------------------
	{
		$sttThumb = self::normalizeThumb($varThumb);

		$arrName = explode('/', $this->source);  
		$strDirName = $arrName[0];	
		$strSrcName = $arrName[1];

		$intImageType = 0;
		if (!($resImage = $this->getResource($intImageType))) 
			return false;

		$strDstName = $strSrcName . '_' . $sttThumb['suffix'] . ($intImageType == IMAGETYPE_PNG ? '.png' : '.jpg');
		$strDstPath = $this->module->assetsPath . '/' . $strDirName . '/' . $strDstName;
		$strDstUrl = $this->module->assetsUrl . '/' . $strDirName . '/' . $strDstName;

		if (!is_file($strDstPath) || Yii::app()->getModule('files')->debug) {

			if (!is_dir($this->module->assetsPath . '/' . $strDirName)) {
				mkdir($this->module->assetsPath . '/' . $strDirName);
				chmod($this->module->assetsPath . '/' . $strDirName, 0777);
			}


			if ($sttThumb['mode'] == THUMB_MODE_COPY) {
				copy($this->getSourcePath(), $strDstPath);
				chmod($strDstPath, 0777);				
			} else {

				$arrHandlers = array(
					THUMB_MODE_CROP => 'thumbCrop',
					THUMB_MODE_FILL => 'thumbFill',
					THUMB_MODE_GRIP => 'thumbGrip',
					);

				// Создать миниатюру
				$strHandle = $arrHandlers[$sttThumb['mode']];
				$resDstImage = $this->$strHandle($resImage, $sttThumb['width'], $sttThumb['height']);

				// Сохранить файл
				if($intImageType == IMAGETYPE_PNG) imagepng($resDstImage, $strDstPath);
				else imagejpeg($resDstImage, $strDstPath, 90);
				chmod($strDstPath, 0777);				

			}

		}

		return $strDstUrl;
	}

	//----------------------------------------------------------------------------
	public static function normalizeThumb($thumb)
	//----------------------------------------------------------------------------
	{
		if (is_array($thumb)) $sttThumb = $thumb;
		else {
			$arrThumbs = Yii::app()->getModule('files')->thumbs;
			$sttThumb = $arrThumbs[$thumb];
			$sttThumb['suffix'] = $thumb;
		}

		if (!isset($sttThumb['mode'])) $sttThumb['mode'] = THUMB_MODE_CROP;
		if (!isset($sttThumb['width'])) $sttThumb['width'] = 0;
		if (!isset($sttThumb['height'])) $sttThumb['height'] = 0;
		if (!isset($sttThumb['suffix'])) $sttThumb['suffix'] = $sttThumb['width'] . '_' . $sttThumb['height'] . '_' . $sttThumb['mode'];
		if (!isset($sttThumb['noimageUrl'])) $sttThumb['noimageUrl'] = Yii::app()->getModule('files')->noimageUrl;

		return $sttThumb; 
	}

	//----------------------------------------------------------------------------
	private function prepValues($resImage, $dstWidth, $dstHeight)
	//----------------------------------------------------------------------------
	{
		$srcWidth = imagesx($resImage);
		$srcHeight = imagesy($resImage);
		$srcRatio = $srcWidth / $srcHeight;

		if($dstHeight == 0) $dstHeight = $dstWidth / $srcRatio;
		if($dstWidth == 0) $dstWidth = $dstHeight * $srcRatio;

		$dstRatio = $dstWidth / $dstHeight;

		return compact('srcWidth', 'srcHeight', 'srcRatio', 'dstWidth', 'dstHeight', 'dstRatio');
	}

	//----------------------------------------------------------------------------
	public function getResource(&$intType)
	//----------------------------------------------------------------------------
	{
		$src = $this->getSourcePath();

		if (!is_file($src)) return false;

		$t = getimagesize($src);

		$intType = $t[2];
		if($intType == 1) return @imagecreatefromgif($src);
		if($intType == 2) return @imagecreatefromjpeg($src);
		if($intType == 6) return @imagecreatefromwbmp($src);
		if($intType == IMAGETYPE_PNG) return @imagecreatefrompng($src);

		return false;
	}

	//----------------------------------------------------------------------------
	public function thumbCrop($resImage, $dstWidth, $dstHeight)
	//----------------------------------------------------------------------------
	{
		extract($this->prepValues($resImage, $dstWidth, $dstHeight));

		if($srcRatio < $dstRatio)
		{
			// Ширина старая, обрезаем по высоте
			$tmpWidth = $srcWidth;
			$tmpHeight = $srcWidth / $dstRatio;
			$tmpX = 0;
			$tmpY = ($srcHeight-$tmpHeight)/2;
		}
		else
		{
			// Высота старая, обрезаем по ширине
			$tmpHeight = $srcHeight;				
			$tmpWidth = $srcHeight * $dstRatio;		
			$tmpX = ($srcWidth-$tmpWidth)/2;
			$tmpY = 0;
		}

		$resDstImage = imagecreatetruecolor($dstWidth, $dstHeight);
		imagecopyresampled($resDstImage, $resImage, 0, 0, $tmpX, $tmpY, $dstWidth, $dstHeight, $tmpWidth, $tmpHeight);
		return $resDstImage;
	}

	//----------------------------------------------------------------------------
	public function thumbFill($resImage, $dstWidth, $dstHeight)
	//----------------------------------------------------------------------------
	{
		extract($this->prepValues($resImage, $dstWidth, $dstHeight));

		if($srcWidth > $srcHeight)
		{
			$tmpWidth = $dstWidth;
			$tmpHeight = $tmpWidth / $srcRatio;
			$tmpX = 0;
			$tmpY = ($dstHeight - $tmpHeight) / 2;
		}
		else
		{
			$tmpHeight	=	$dstHeight;
			$tmpWidth	=	$tmpHeight / $srcRatio;
			$tmpY	=	0;
			$tmpX	=	($dstWidth - $tmpWidth) / 2;
		}

		$resDstImage = imagecreatetruecolor($dstWidth,$dstHeight);
		$bg = imagecolorallocate($resDstImage, 255, 255, 255);
		imagefill($resDstImage, 1, 1, $bg);
		imagecopyresampled ($resDstImage, $resImage, $tmpX, $tmpY, 0, 0, $tmpWidth, $tmpHeight, $srcWidth, $srcHeight);		
		return $resDstImage;
	}

	//----------------------------------------------------------------------------
	public function thumbGrip($resImage, $dstWidth, $dstHeight)
	//----------------------------------------------------------------------------
	{
		extract($this->prepValues($resImage, $dstWidth, $dstHeight));

		if($srcWidth > $srcHeight) {
			$tmpWidth = $dstWidth;
			$tmpHeight = $tmpWidth/$srcRatio;
			$tmpX = 0;
			$tmpY = ($dstHeight-$tmpHeight)/2;
		}
		else {
			$tmpHeight=$dstHeight;
			$tmpWidth=$tmpHeight*$srcRatio;
			$tmpY=0;
			$tmpX=($dstWidth-$tmpWidth)/2;
		}

		$resDstImage = imagecreatetruecolor($tmpWidth,$tmpHeight);
		imagecopyresampled ($resDstImage, $resImage, 0, 0, 0, 0, $tmpWidth, $tmpHeight, $srcWidth, $srcHeight);		
		return $resDstImage;

	}

	//****************************************************************************
	// Вспомогательные функции
	//****************************************************************************

	//----------------------------------------------------------------------------
	public static function createByFile($objFile)
	//----------------------------------------------------------------------------
	{
		$modFile = new File;
		$modFile->name = $objFile->name;
		$modFile->source = $objFile;
		return $modFile;				
	}

	//----------------------------------------------------------------------------
	public function getModule()
	//----------------------------------------------------------------------------
	{
		return Yii::app()->getModule('files');
	}

	//----------------------------------------------------------------------------
	public function getSourcePath()
	//----------------------------------------------------------------------------
	{
		return $this->module->uploadPath . '/' . $this->source;
	}

	public static function model($className=__CLASS__) {return parent::model($className);}
	public function tableName() {return 'tbl_files';}
}
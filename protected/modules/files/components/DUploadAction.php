<?php

# ver: 2.0.0

//------------------------------------------------------------------------------
class DUploadAction extends CAction
//------------------------------------------------------------------------------
{
	public $thumb;	// Миниатюра, которая будет использована

	//----------------------------------------------------------------------------
	public function run()
	//----------------------------------------------------------------------------
	{
		if ($objFile = CUploadedFile::getInstanceByName('file'))
		{
			$modImage = File::createByFile($objFile);

			if ($modImage->save()) {
				$arrRet = array(
					'filelink' => $modImage->publish($this->thumb),
					'fileId' => $modImage->id,
				);

				echo stripslashes(json_encode($arrRet));
			} 
		}		
	}	
}

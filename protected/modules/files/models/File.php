<?php

# ver: 2.0.0

//******************************************************************************
class File extends DFile
//******************************************************************************
{
	public $request; // Поисковый запрос




	//****************************************************************************
	// AR - методы
	//****************************************************************************

	//----------------------------------------------------------------------------
	public function attributeLabels()
	//----------------------------------------------------------------------------  
	{
		return array(
			'name' => 'Имя',
			'tst_upload' => 'Загружен',
		);
	}

	//----------------------------------------------------------------------------
	public function rules()
	//----------------------------------------------------------------------------	
	{
		return array(
				array('source', 'required'),
				array('name, source', 'length', 'max' => 255),
				array('tst_upload', 'length', 'max' => 10),

				array('request', 'safe', 'on'=>'search'),
		);
	}

	//----------------------------------------------------------------------------
	public function relations()
	//----------------------------------------------------------------------------
	{
		// ВНИМАНИЕ: уточните имя связи
		return array(
		);
	}	

	//****************************************************************************
	// Поиск
	//****************************************************************************

	//----------------------------------------------------------------------------
	public function search()
	//----------------------------------------------------------------------------
	{
		// Внимание: удалите лишние атрибуты!

		$objCriteria = new CDbCriteria;

		// Фильтрация полей
		$objCriteria->compare('id', $this->id, true);
		$objCriteria->compare('name', $this->name, true);
		$objCriteria->compare('source', $this->source, true);
		$objCriteria->compare('tst_upload', $this->tst_upload, true);

		// Запрос по строке
		$objCriteria->compare('id', $this->request, false, 'OR');
		$objCriteria->compare('name', $this->request, false, 'OR');
		$objCriteria->compare('source', $this->request, false, 'OR');
		$objCriteria->compare('tst_upload', $this->request, false, 'OR');


		return new CActiveDataProvider($this, array(
			'criteria' => $objCriteria,
			'sort' => false,
			'pagination' => array('pageSize' => 25),
		));
	}

	public static function model($className=__CLASS__) {return parent::model($className);}
}
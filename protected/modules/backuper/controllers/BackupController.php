<?php

# ver: 2.0.0

class BackupController extends DcController
{
	public $defaultAction = 'admin';                

	public $titles = array(
		'admin' => 'Резервные копии',
		'create' => 'Сделать бекап',
		);

	//----------------------------------------------------------------------------
	public function actionAdmin()
	//----------------------------------------------------------------------------
	{
		$modBackup = new BackuperBackup;

		$this->menu = array(                          
			array('label' => 'Опции'), 
			array('label' => $this->getTitle('create'), 
					'icon' => 'plus',
					'url' => array($r = 'create'), 
					'visible' => $this->checkAccessOnce($r)),
		);
						
		$this->render('admin', array('modBackup' => $modBackup));
	}
	
	//----------------------------------------------------------------------------
	public function actionCreate()
	//----------------------------------------------------------------------------  
	{
		$modBackup = new BackuperBackup;
		$modBackup->server_name = $this->module->serverName;

		// Выполнить сохранение
		if(isset($_POST['BackuperBackup']))
		{
			$modBackup->attributes = $_POST['BackuperBackup'];
			if($modBackup->save()) {

				Yii::app()->user->setFlash('info', "Бекап <strong>$modBackup->id</strong> успешно создан");
				$this->redirectPageStatePrevious();
			}
		}
		else $this->initPageStatePrevious();

		$this->addBreadcrumb('admin');

		// Передать контроллеру    
		$this->render('form', array('modBackup' => $modBackup));
	}

	//----------------------------------------------------------------------------
	public function actionView($urlBackup)
	//----------------------------------------------------------------------------  
	{
		$modBackup = $this->loadModel($urlBackup);
		
		$this->menu = array(   
			array('label' => 'Опции'), 
			array(
				'label'=>'Восстановить бекап', 
				'icon' => 'refresh', 
				'url' => '#',
				'linkOptions' => array('submit' => array($r = 'retreive', 'urlBackup' => $urlBackup), 'confirm'=>'Точно?'),
				'visible' => $this->checkAccessOnce($r),
				),
		);                       

		$this->addBreadcrumb('admin');

		$this->render('view', array('model' => $modBackup, 'attributes' => null));
	}

	//----------------------------------------------------------------------------
	public function actionRetreive($urlBackup)
	//----------------------------------------------------------------------------  
	{
		$objBackup = new BackuperBackup;

		if (!$objBackup->retreiveByPk($urlBackup)) 
			Yii::app()->user->setFlash('error', '<strong>Бекап ' . $urlBackup . ' не восстановлен: </strong><br>' . $objBackup->error);
		else Yii::app()->user->setFlash('info', 'Бекап ' . $urlBackup . ' восстановлен');
		
		$this->redirect(array('admin'));
	}
	
	//----------------------------------------------------------------------------  
	public function actionDelete($urlBackup)
	//----------------------------------------------------------------------------  
	{
		if(Yii::app()->request->isPostRequest)
		{
			$objBackup = new BackuperBackup;
			$modBackup = $objBackup->deleteByPk($urlBackup);

			// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
			if(!isset($_GET['ajax']))
			{
				$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
			}
		}
		else throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
	}

	//----------------------------------------------------------------------------
	public function loadModel($urlBackup)
	//----------------------------------------------------------------------------
	{
		$objBackup = new BackuperBackup; 
		$modBackup = $objBackup->findByPk($urlBackup);

		if ($modBackup === null)
			throw new CHttpException(404, 'Бекап не найден');
		
		$this->titles['view'] = $modBackup->id;

		return $modBackup;
	}
}
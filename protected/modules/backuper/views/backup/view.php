<?php 

	# ver: 2.0.0

?>


<div class="row-fluid">

	<div class="span7">
		<table class='table table-bordered table-striped' style='margin: 0px auto;'>
			<tr>
				<th>Таблица</th>
				<th style='width: 20px;'>&nbsp;</th>
			</tr>


			<?php foreach ($model->enumTables as $strTable): ?>
			<tr>
				<td><?php echo $strTable; ?></td>
				<td>
					<?php if (in_array($strTable, $model->tables)): ?>
						<i class='icon-ok'></i>
					<?php endif; ?>
				</td>
			</tr>
			
			<?php endforeach; ?>
		</table>

	</div>

	<div class="span5">		
		<?php $this->widget('bootstrap.widgets.TbDetailView', array(
			'data' => $model,
			'attributes'=> array('name', 'date:datetime', 'user'),
		)); ?>
	</div>	
</div>





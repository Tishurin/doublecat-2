<?php 

	# ver: 2.0.0

?>

<?php 
	$this->widget('bootstrap.widgets.TbGridView', array(
	'type'=>'striped bordered',
	'template' => '{items}{pager}',
	'dataProvider' => new CArrayDataProvider($modBackup->findAll(), array('sort' => false, 'pagination' => false)),
	'ajaxUpdate' => false,
	'selectableRows' => 0,
	'columns' => array(
		array('name' => 'id', 'header' => 'ID', 'htmlOptions' => array('style' => 'width: 100px;')),
		array('name' => 'name', 'header' => 'Имя', 'htmlOptions' => array()),
		array('name' => 'date', 'type' => 'datetime', 'header' => 'Создан', 'htmlOptions' => array('style' => 'width: 120px;')),

		array('class' => 'bootstrap.widgets.TbButtonColumn', 
				'htmlOptions' => array('style'=>'width: 10px'),
				'visible' => Yii::app()->user->checkAccess('file/admin'),
				'template' => '{retreive}&nbsp;{view}&nbsp;{delete}',
				'viewButtonUrl' => 'array("view", "urlBackup" => $data->id)',         
				'deleteButtonUrl' => 'array("delete", "urlBackup" => $data->id)',

				'buttons' => array(
					'retreive' => array(
						'icon' => 'refresh',
						'url' => 'array("retreive", "urlBackup" => $data->id)',
						'label' => 'Восстановить',
						'click' => 'function() {if (confirm("Точно?")) return true; else return false;}',
						),
				
					),  
						 
				),          
)
	)); 
?>

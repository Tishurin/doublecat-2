<?php

# ver: 2.0.0

class DWebUser extends CWebUser 
{
	private $_model = null;


	//----------------------------------------------------------------------------  
	public function init()
	//----------------------------------------------------------------------------  
	{
		parent::init();

		if (!$this->isGuest && $this->model === null) $this->logout();


		if (!$this->isGuest && $this->model->date_login < strtotime('today'))
		{
			$this->model->date_login = time();
			$this->model->save();
		}
	}

	//----------------------------------------------------------------------------  
	public function getRole() 
	//----------------------------------------------------------------------------  
	{
		return $this->model !== null ? $this->model->role : null;
	}
	
	//----------------------------------------------------------------------------  
	public function getModel()
	//----------------------------------------------------------------------------
	{    
		if (!$this->isGuest && $this->_model === null)
			$this->_model = User::model()->findByPk($this->id);

		return $this->_model;
	}
}
<?php

# ver: 2.0.0

class DUserIdentity extends CUserIdentity
{
	private $_id;
	
	const ERROR_USER_BLOCKED = 11;

	//----------------------------------------------------------------------------
	public function authenticate()
	//----------------------------------------------------------------------------
	{
		$modUser = User::model()->find('login = :login OR email = :email', array('login' => $this->username, 'email' => $this->username));
		
		if($modUser === null) $this->errorCode = self::ERROR_USERNAME_INVALID;
		elseif($modUser->password !== md5($modUser->salt . $this->password)) $this->errorCode=self::ERROR_PASSWORD_INVALID;
		else
		{
			// Пользователь авторизован
			$this->_id = $modUser->id;
			$this->setState('name', $modUser->name);
			$this->errorCode=self::ERROR_NONE;
		}
		return !$this->errorCode;

	}

	//----------------------------------------------------------------------------
	public function getId()
	//----------------------------------------------------------------------------  
	{
		return $this->_id;
	}

}
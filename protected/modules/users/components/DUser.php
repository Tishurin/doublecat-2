<?php

# ver: 2.0.0.1
# 2.0.0.1 - добавлена сортировка по имени

//******************************************************************************
class DUser extends DActiveRecord
//******************************************************************************
{
	public $request; // Поисковый запрос

	public $active = 1;

	public $confirm;

	private $_enum_role;


	//****************************************************************************
	// AR - методы
	//****************************************************************************

	//----------------------------------------------------------------------------
	public function attributeLabels()
	//----------------------------------------------------------------------------  
	{
		return array(
			'name' => 'Имя',
			'login' => 'Логин',
			'email' => 'Email',
			'password' => 'Пароль',
			'date_login' => 'Заходил',
			'active' => 'Активен',
			'confirm' => 'Подтверждение',
			'role' => 'Права',
		);
	}

	//----------------------------------------------------------------------------
	public function rules()
	//----------------------------------------------------------------------------	
	{
		return array(
				array('name, email, role', 'required'),
				array('login, email', 'unique'),
				array('email', 'email'),

				array('name, login, email, role', 'length', 'max'=>255),
				array('active', 'length', 'max'=>11),
	
				array('password, confirm', 'required', 'on' => array('insert', 'password')),
				array('confirm', 'validatePassword', 'on' => array('insert', 'password')),

				// Внимание: удалите лишние атрибуты!
				array('request', 'safe', 'on'=>'search'),
		);
	}

	//----------------------------------------------------------------------------
	public function relations()
	//----------------------------------------------------------------------------
	{
		// ВНИМАНИЕ: уточните имя связи
		return array(
		);
	}

	//****************************************************************************
	// Пользовательские методы
	//****************************************************************************

	//----------------------------------------------------------------------------
	public function validatePassword()
	//----------------------------------------------------------------------------
	{
		if ($this->password != $this->confirm) {
			$this->addError('password', 'Введенные пароль должны быть более похожи...');
			$this->password = '';
			$this->confirm = '';
			return false;			
		} else {
			$this->salt = uniqid();
			$this->password = md5($this->salt . $this->password);			
		}

		return true;
	}

	//----------------------------------------------------------------------------
	public function getEnum_role()
	//----------------------------------------------------------------------------  
	{
		if ($this->_enum_role == null)
		{
			$this->_enum_role = array();
			foreach (Yii::app()->authManager->roles as $url_role => $obj_role)
			$this->_enum_role[$url_role] = $obj_role->description;
		}

		return $this->_enum_role;
	}

	//****************************************************************************
	// Поиск
	//****************************************************************************

	//----------------------------------------------------------------------------
	public function search()
	//----------------------------------------------------------------------------
	{
		// Внимание: удалите лишние атрибуты!

		$objCriteria = new CDbCriteria;

		// Запрос по строке
		$objCriteria->compare('name', $this->request, false, 'OR');
		$objCriteria->compare('login', $this->request, false, 'OR');
		$objCriteria->compare('email', $this->request, false, 'OR');

		$objCriteria->order = 'name';

		return new CActiveDataProvider($this, array(
			'criteria' => $objCriteria,
			'pagination' => false,
			'sort' => false,
		));
	}


	public static function model($className=__CLASS__) {return parent::model($className);}
	public function tableName() {return 'tbl_users';}
}
<?php 
	# ver: 2.0.0
?>

<?php $form = $this->beginWidget('DActiveForm', array(
	'id'=>'user-form',
	'stateful' => true,
	'htmlOptions' => array('class' => 'form-horizontal',
												 'enctype' => 'multipart/form-data'),
	'enableAjaxValidation' => false,
	'enableClientValidation' => false,
	'focus' => array($modUser, 'name'),	
)); ?>

	<fieldset>

		<div class="control-group">
			<div class='controls'>          
				<?php if(count($modUser->errors) > 0): ?>
					<div class="span12 alert alert-error">
					 <?php echo $form->errorSummary($modUser); ?>
					</div>
				<?php endif ?>
			</div>
		</div>
		
		<!-- NAME -->
		<div class="control-group <?php if (isset($modUser->errors['name'])) echo 'error'; ?> ">
			<?php echo $form->labelEx($modUser, 'name', array('class' => 'control-label')); ?>
			<div class='controls'>
				<?php echo $form->textField($modUser, 'name', array('class' => 'span12')); ?>
				<?php echo $form->error($modUser, 'name', array('class' => 'help-block')); ?>
			</div>
		</div>
				
		<div class="row-fluid">
			<div class="span6">
				<!-- EMAIL -->
				<div class="control-group <?php if (isset($modUser->errors['email'])) echo 'error'; ?> ">
					<?php echo $form->labelEx($modUser, 'email', array('class' => 'control-label')); ?>
					<div class='controls'>
						<?php echo $form->textField($modUser, 'email', array('class' => 'span12')); ?>
						<?php echo $form->error($modUser, 'email', array('class' => 'help-block')); ?>
					</div>
				</div>				
				

			</div>
			<div class="span6">

				<!-- LOGIN -->
				<div class="control-group <?php if (isset($modUser->errors['login'])) echo 'error'; ?> ">
					<?php echo $form->labelEx($modUser, 'login', array('class' => 'control-label')); ?>
					<div class='controls'>
						<?php echo $form->textField($modUser, 'login', array('class' => 'span12')); ?>
						<?php echo $form->error($modUser, 'login', array('class' => 'help-block')); ?>
					</div>
				</div>

				
			</div>
		</div>

		<?php if ($modUser->isNewRecord): ?>
			<div class="row-fluid">
				<div class="span6">				
					<!-- PASSWORD -->
					<div class="control-group <?php if (isset($modUser->errors['password'])) echo 'error'; ?> ">
						<?php echo $form->labelEx($modUser, 'password', array('class' => 'control-label')); ?>
						<div class='controls'>
							<?php echo $form->passwordField($modUser, 'password', array('class' => 'span12')); ?>
							<?php echo $form->error($modUser, 'password', array('class' => 'help-block')); ?>
						</div>
					</div>				
				</div>
				<div class="span6">
					<!-- confirm -->
					<div class="control-group <?php if (isset($modUser->errors['confirm'])) echo 'error'; ?> ">
						<?php echo $form->labelEx($modUser, 'confirm', array('class' => 'control-label')); ?>
						<div class='controls'>
							<?php echo $form->passwordField($modUser, 'confirm', array('class' => 'span12')); ?>
							<?php echo $form->error($modUser, 'confirm', array('class' => 'help-block')); ?>
						</div>
					</div>				

				</div>
			</div>
		<?php endif ?>

		<?php if (Yii::app()->user->checkAccess('users/user')): ?>

			<div class="row-fluid">

				<div class="span6">
					
					<!-- role -->
					<div class="control-group <?php if (isset($modUser->errors['role'])) echo 'error'; ?> ">
						<?php echo $form->labelEx($modUser, 'role', array('class' => 'control-label')); ?>
						<div class='controls'>
							<?php echo $form->dropDownList($modUser, 'role', array('Укажите права доступа') + $modUser->enum_role, array('class' => 'span12')); ?>
							<?php echo $form->error($modUser, 'role', array('class' => 'help-block')); ?>
						</div>
					</div>

				</div>

				<div class="span6">
					
					<!-- ACTIVE -->
					<div class="control-group <?php if (isset($modUser->errors['active'])) echo 'error'; ?> ">
						<?php echo $form->labelEx($modUser, 'active', array('class' => 'control-label')); ?>
						<div class='controls'>
							<?php echo $form->checkBox($modUser, 'active', array('class' => 'span12')); ?>
							<?php echo $form->error($modUser, 'active', array('class' => 'help-block')); ?>
						</div>
					</div>

				</div>			
			</div>

		<?php endif ?>

		

		
	</fieldset>

	<div class="form-actions">
		<?php echo TbHtml::linkButton('Отмена', array('color' => TbHtml::BUTTON_COLOR_DANGER, 'url' => $this->pageStatePrevious, 'icon' => 'remove white')); ?> 
		<?php echo TbHtml::submitButton('Готово', array('color' => TbHtml::BUTTON_COLOR_PRIMARY, 'icon' => 'ok white')); ?> 
	</div>


<?php $this->endWidget(); ?>

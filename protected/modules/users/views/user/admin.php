<?php 
	# ver: 2.0.0
?>

<?php $form=$this->beginWidget('CActiveForm', array(
	'action' => Yii::app()->createUrl($this->route),
	'method' => 'get',
	'htmlOptions' => array('style' => 'margin-bottom: 0;'),
)); ?>
	<div class='well well-small' style='padding-right: 130px; margin-bottom: 15px;'>
		<?php echo TbHtml::button('Искать', array('color' => TbHtml::BUTTON_COLOR_PRIMARY, 'type' => TbHtml::BUTTON_TYPE_SUBMIT, 'icon' => 'icon-search icon-white', 'style' => 'float: right; text-align: left; margin-right: -120px; width: 100px;')); ?> 
		<?php echo $form->textField($modUser, 'request', array('style' => 'margin-bottom: 0; width: 100%;', 'placeholder' => 'Введите поисковое слово')); ?> 
	</div>
<?php $this->endWidget(); ?>

<?php $this->widget('bootstrap.widgets.TbGridView', array(
	'id' => 'user-grid',
	'dataProvider' => $modUser->search(),
	'type'=>'striped bordered',
	'template' => '{items}{pager}',
	'selectableRows' => 0,
	'emptyText' => 'Ничего не нашлось...',
	'ajaxUpdate' => false,
	'columns'=>array(
		array('name' => 'name', 'htmlOptions' => array()),
		array('name' => 'email', 'htmlOptions' => array()),
		array('name' => 'enum.role', 'htmlOptions' => array()),
		array('name' => 'active', 'type' => 'boolean', 'htmlOptions' => array('class' => 'date')),
		array('name' => 'date_login', 'type' => 'date', 'htmlOptions' => array('class' => 'date')),
		array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
			'template' => '{password} {update} {delete}',
			'htmlOptions' => array('style' => 'width: 50px;'),
			'buttons' => array(
				'password' => array(
						'icon' => 'lock',
						'label' => 'Сменить пароль',
						'url' => 'array("password", "id" => $data->id)',
					)
			),
		),
	),
)); ?>

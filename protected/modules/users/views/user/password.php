<?php 
	# ver: 2.0.0
?>

<?php $form = $this->beginWidget('DActiveForm', array(
	'id'=>'user-form',
	'stateful' => true,
	'htmlOptions' => array('class' => 'form-horizontal',
												 'enctype' => 'multipart/form-data'),
	'enableAjaxValidation' => false,
	'enableClientValidation' => false,
	'focus' => array($modUser, 'password'),
)); ?>

	<fieldset>

		<div class="control-group">
			<div class='controls'>          
				<?php if(count($modUser->errors) > 0): ?>
					<div class="span10 alert alert-error">
					 <?php echo $form->errorSummary($modUser); ?>
					</div>
				<?php endif ?>
			</div>
		</div>
		
		<div class="row-fluid">
			<div class="span6">				
				<!-- PASSWORD -->
				<div class="control-group <?php if (isset($modUser->errors['password'])) echo 'error'; ?> ">
					<?php echo $form->labelEx($modUser, 'password', array('class' => 'control-label')); ?>
					<div class='controls'>
						<?php echo $form->passwordField($modUser, 'password', array('class' => 'span12')); ?>
						<?php echo $form->error($modUser, 'password', array('class' => 'help-block')); ?>
					</div>
				</div>				
			</div>
			<div class="span6">
				<!-- confirm -->
				<div class="control-group <?php if (isset($modUser->errors['confirm'])) echo 'error'; ?> ">
					<?php echo $form->labelEx($modUser, 'confirm', array('class' => 'control-label')); ?>
					<div class='controls'>
						<?php echo $form->passwordField($modUser, 'confirm', array('class' => 'span12')); ?>
						<?php echo $form->error($modUser, 'confirm', array('class' => 'help-block')); ?>
					</div>
				</div>				

			</div>
		</div>
		
	</fieldset>

	<div class="form-actions">
		<?php echo TbHtml::linkButton('Отмена', array('color' => TbHtml::BUTTON_COLOR_DANGER, 'url' => $this->pageStatePrevious, 'icon' => 'remove white')); ?> 
		<?php echo TbHtml::submitButton('Готово', array('color' => TbHtml::BUTTON_COLOR_PRIMARY, 'icon' => 'ok white')); ?> 
	</div>


<?php $this->endWidget(); ?>

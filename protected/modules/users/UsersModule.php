<?php

# ver: 2.0.0

class UsersModule extends CWebModule
{
	public $defaultReturnUrl;

	public function init()
	{
		$this->defaultReturnUrl = Yii::app()->getModule('cp')->defaultController;

		$this->setImport(array(
			'users.models.*',
			'users.components.*',
		));
	}

	public function beforeControllerAction($controller, $action)
	{
		if(parent::beforeControllerAction($controller, $action))
		{
			// this method is called before any module controller action is performed
			// you may place customized code here
			return true;
		}
		else
			return false;
	}
}

<?php

# ver: 2.0.0

class LoginController extends DcController
{

  	//----------------------------------------------------------------------------
	public function actionLogin()
  	//----------------------------------------------------------------------------
	{
		$this->pageTitle = 'Вход в систему';
		$this->showBreadcrumbs = false;
		$this->showHeader = false;
		
	    if (Yii::app()->user->isGuest) 
	    {
			$modLogin = new UserLogin;

			if(isset($_POST['UserLogin']))
			{
				$modLogin->attributes = $_POST['UserLogin'];
				
				if($modLogin->validate()) $this->redirect(Yii::app()->user->getReturnUrl($this->module->defaultReturnUrl));
				else $modLogin->password = '';
			}
				
			$this->render('login', array('modLogin'=>$modLogin));
		} 
	    else $this->redirectBack();
	}

	//----------------------------------------------------------------------------
	public function actionLogout()
	//----------------------------------------------------------------------------
	{
		Yii::app()->user->logout();
		$this->redirect(Yii::app()->user->loginUrl);
	}

}
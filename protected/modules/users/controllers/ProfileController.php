<?php

# ver: 2.0.0

class ProfileController extends DcController
{
	public $defaultAction = 'view';

	public $titles = array(
		'view' => 'Мой профиль',
		'password' => 'Сменить пароль',
		);

	//----------------------------------------------------------------------------
	public function actionView()
	//----------------------------------------------------------------------------
	{
		$modProfile = $this->loadModel();

		if(isset($_POST['UserProfile']))
		{
			$modProfile->attributes = $_POST['UserProfile'];
			if($modProfile->save()) {
				Yii::app()->user->setFlash('info', 'Новые учетные данные сохранены');				
				$this->redirect(array('view'));
			}
		}
		else $this->initPageStatePrevious(array('view')); // Переопределите если нужно

		$this->menu = array(                          
			array('label' => 'Опции'), 
			array('label' => $this->getTitle('password'), 
						'icon' => 'lock',
						'url' => array($r = 'password'), 
						'visible' => $this->checkAccessOnce($r),
			),
		);		

		$this->render('/user/_form', array('modUser' => $modProfile));
	}

	//----------------------------------------------------------------------------
	public function actionPassword()
	//----------------------------------------------------------------------------
	{
		$modProfile = $this->loadModel();
		$modProfile->scenario = 'password';

		$modProfile->password = '';

		if(isset($_POST['UserProfile']))
		{
			$modProfile->attributes = $_POST['UserProfile'];
			if($modProfile->save()) {
				Yii::app()->user->setFlash('info', 'Новый пароль сохранен');							
				$this->redirectPageStatePrevious();				
			}
		}
		else $this->initPageStatePrevious(array('view', 'id' => $modProfile->id)); // Переопределите если нужно

		$this->addBreadcrumb('view');
		$this->render('/user/password', array('modUser' => $modProfile));
	}

	//----------------------------------------------------------------------------
	public function loadModel()
	//----------------------------------------------------------------------------
	{
		$modProfile = UserProfile::model()->findByPk(Yii::app()->user->id);
		
		if($modProfile === null)
			throw new CHttpException(404,'Такой страницы нет!');

		return $modProfile;
	}
}

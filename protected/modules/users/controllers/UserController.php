<?php

# ver: 2.0.0

class UserController extends DcController
{
	public $defaultAction = 'admin';

	public $titles = array(
		'admin' => 'Учетные записи',
		'update' => 'Редактировать',
		'password' => 'Сменить пароль',
		'create' => 'Новый пользователь',
		);

	//----------------------------------------------------------------------------
	public function actionAdmin()
	//----------------------------------------------------------------------------
	{
		$modUser = new User('search');
		$modUser->unsetAttributes();
		
		if(isset($_GET['User'])) $modUser->attributes = $_GET['User'];

		$this->menu = array(                          
			array('label' => 'Опции'), 
			array('label' => $this->getTitle('create'), 
					'icon' => 'plus',
					'url' => array($r = 'create'), 
					'visible' => $this->checkAccessOnce($r),
			),
		);


		$this->render('admin', array('modUser' => $modUser));
	}

	//----------------------------------------------------------------------------
	public function actionCreate()
	//----------------------------------------------------------------------------
	{
		$modUser = new User;

		if(isset($_POST['User']))
		{
			$modUser->attributes = $_POST['User'];
			if($modUser->save())
				$this->redirectPageStatePrevious();
		}
		else $this->initPageStatePrevious();

		$this->addBreadcrumb('admin');

		$this->render('create', array('modUser' => $modUser));
	}

	//----------------------------------------------------------------------------
	public function actionUpdate($id)
	//----------------------------------------------------------------------------
	{
		$modUser = $this->loadModel($id);

		if(isset($_POST['User']))
		{
			$modUser->attributes = $_POST['User'];
			if($modUser->save())
				$this->redirectPageStatePrevious();
		}
		else $this->initPageStatePrevious(array('view', 'id' => $modUser->id)); // Переопределите если нужно


		$this->addBreadcrumb('admin');

		$this->render('update', array('modUser' => $modUser));
	}

	//----------------------------------------------------------------------------
	public function actionPassword($id)
	//----------------------------------------------------------------------------
	{
		$modUser = $this->loadModel($id);
		$modUser->scenario = 'password';

		$modUser->password = '';

		if(isset($_POST['User']))
		{
			$modUser->attributes = $_POST['User'];
			if($modUser->save())
				$this->redirectPageStatePrevious();
		}
		else $this->initPageStatePrevious(array('view', 'id' => $modUser->id)); // Переопределите если нужно

		$this->addBreadcrumb('admin');
		$this->addUpdateBreadcrumb($modUser);

		$this->render('password', array('modUser' => $modUser));
	}

	//----------------------------------------------------------------------------
	public function actionDelete($id)
	//----------------------------------------------------------------------------  
	{
		if(Yii::app()->request->isPostRequest)
		{
			// we only allow deletion via POST request
			$this->loadModel($id)->delete();

			// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
			if(!isset($_GET['ajax']))
				$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
		}
		else
			throw new CHttpException(400,'Ошибка запроса.');
	}

	//----------------------------------------------------------------------------
	public function addUpdateBreadcrumb($modUser)
	//----------------------------------------------------------------------------
	{
		$this->addBreadcrumb('update', array('update', 'id' => $modUser->id));
	}

	//----------------------------------------------------------------------------
	public function loadModel($id)
	//----------------------------------------------------------------------------
	{
		$modUser = User::model()->findByPk($id);
		if($modUser === null)
			throw new CHttpException(404,'Такой страницы нет!');

		$this->titles['update'] = $modUser->name; 	
		return $modUser;
	}
}

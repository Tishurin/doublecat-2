<?php

# ver: 2.0.0

//******************************************************************************
class UserProfile extends User
//******************************************************************************
{

	//----------------------------------------------------------------------------
	public function rules()
	//----------------------------------------------------------------------------	
	{
		return array(
			array('name, email', 'required'),
			array('login, email', 'unique'),
			array('email', 'email'),

			array('name, login, email', 'length', 'max'=>255),

			array('password, confirm', 'required', 'on' => array('password')),
			array('confirm', 'validatePassword', 'on' => array('password')),
		);
	}

	public static function model($className=__CLASS__) {return parent::model($className);}
}
<?php

# ver: 2.0.0

class SettingsController extends DcController
{
	public $defaultAction = 'view';
	public $titles = array(
		'view' => 'Настройки',
		);

	//----------------------------------------------------------------------------	 
	public function actionView()
	//----------------------------------------------------------------------------	 	
	{
		$modSettings = new Settings;

		if (isset($_POST['Settings']))
		{
			$modSettings->attributes = $_POST['Settings'];

			if ($modSettings->save()) {
				Yii::app()->user->setFlash('info', 'Настройки сохранены');
				$this->redirectBack();				
			}			
		}

		$this->render('view', array('modSettings' => $modSettings));
	}
}
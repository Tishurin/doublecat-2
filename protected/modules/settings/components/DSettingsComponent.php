<?php

# ver: 2.0.0

class DSettingsComponent extends CApplicationComponent
{
	private $_attributes;
	public $settingsPath;
	public $items;
	
	//----------------------------------------------------------------------------	 
	public function init()
	//----------------------------------------------------------------------------	 
	{
		parent::init();

		$this->settingsPath = Yii::getPathOfAlias('application.runtime') . '/settings.txt';

		if (!is_file($this->settingsPath)) 
			file_put_contents($this->settingsPath, '');

		@$this->_attributes = unserialize(file_get_contents($this->settingsPath));
	}

	//----------------------------------------------------------------------------	 
	public function getAttributes()
	//----------------------------------------------------------------------------	 
	{
		return $this->_attributes;
	}

	//----------------------------------------------------------------------------	 
	public function __get($name)
	//----------------------------------------------------------------------------	 
	// Обращение к именованной настройке
	{		
		if (isset($this->_attributes[$name])) return $this->_attributes[$name];
		else return parent::__get($name);
	}
}

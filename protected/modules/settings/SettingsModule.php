<?php

# ver: 2.0.0

class SettingsModule extends CWebModule
{
	//----------------------------------------------------------------------------	 
	public function init()
	//----------------------------------------------------------------------------	 
	{
		$this->setImport(array(
			'settings.models.*',
			'settings.components.*',
		));
	}
}

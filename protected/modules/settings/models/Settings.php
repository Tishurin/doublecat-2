<?php

# ver: 2.0.0

class Settings extends CFormModel
{
	private $_attributes;
	private $_attributeNames;
	private $_attributeLabels;
	public $formElements;

	//----------------------------------------------------------------------------	 
	public function init()
	//----------------------------------------------------------------------------	 
	{
		$this->normalizeItems();
		$this->attributes = Yii::app()->settings->attributes;
	}

	//----------------------------------------------------------------------------  
	public function normalizeItems()
	//----------------------------------------------------------------------------  
	// Получить настройки, сконфигурировать модель 
	{
		$arrItems = Yii::app()->settings->items;

		foreach ($arrItems as $k => $v) {

				if (!(is_numeric($k) && is_string($v))) {

				$this->_attributeNames[] = $k;

				if (is_string($v)) $v = array('label' => $v);

				if (!isset($v['label'])) $v['label'] = $v['label'] = ucfirst($k);
				if (!isset($v['type'])) $v['type'] = TbHtml::INPUT_TYPE_TEXT;

				$this->_attributeLabels[$k] = $v['label'];

				unset($v['label']);

				if (!isset($v['class'])) $v['class'] = '';
				$v['class'] .= ' span12';
			}

			$this->formElements[$k] = $v;
		}

	}

	//----------------------------------------------------------------------------  
	public function save($runValidation = true, $attributes = null)
	//----------------------------------------------------------------------------  
	{
		if($this->validate($attributes)) {
			return file_put_contents(Yii::app()->settings->settingsPath, serialize($this->attributes));
		}

		else return false;
	}	

	//----------------------------------------------------------------------------  
	public function rules()
	//----------------------------------------------------------------------------  
	// Все настройки безопасны
	{
		return array(
			array(implode(', ', $this->attributeNames()), 'safe'),
		);
	}

	//----------------------------------------------------------------------------  
	public function attributeNames()
	//----------------------------------------------------------------------------
	{
		return $this->_attributeNames;
	}

	//----------------------------------------------------------------------------  
	public function attributeLabels()
	//----------------------------------------------------------------------------  
	{
		return $this->_attributeLabels;
	}

	//----------------------------------------------------------------------------  
	public function __set($name, $value)
	//----------------------------------------------------------------------------  
	{
		if (in_array($name, $this->attributeNames())) $this->_attributes[$name] = $value;
		else parent::__set($name, $value);
	}

	//----------------------------------------------------------------------------  
	public function __get($name)
	//----------------------------------------------------------------------------  
	{
		if (in_array($name, $this->attributeNames())) {
			return isset($this->_attributes[$name]) ? $this->_attributes[$name] : '';
		} 
		else return parent::__get($name);		
	}
}

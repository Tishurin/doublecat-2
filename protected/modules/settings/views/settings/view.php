<!-- 

# ver: 2.0.0

-->

<?php Yii::import('bootstrap.form.*') ?>
<?php Yii::import('bootstrap.widgets.*') ?>


<?php $modForm = new TbForm(
	array('layout' => TbHtml::FORM_LAYOUT_HORIZONTAL,
			'elements' => $modSettings->formElements,
			'buttons' => array(
				'submit' => array(
					'type' => TbHtml::BUTTON_TYPE_SUBMIT,
					'color' => TbHtml::BUTTON_COLOR_PRIMARY, 
					'icon' => 'ok white',
					'label' => 'Готово',
				),
			)), $modSettings) 

?>


<?php echo $modForm->render() ?>


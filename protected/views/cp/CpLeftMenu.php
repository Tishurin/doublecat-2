<?php

# ver: 2.0.p.0

class CpLeftMenu extends TbMenu
{
 	//----------------------------------------------------------------------------
    public function init()
 	//----------------------------------------------------------------------------
	{
		$this->items = array(
			array('label' => 'Главное меню'),
			array('label' => 'Настройки', 'icon' => 'cog', 'url' => array($r = '/settings/settings/view'), 'visible' => Yii::app()->user->checkAccess($r)),

		);
		parent::init();
    }
}

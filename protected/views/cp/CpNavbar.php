<?php

# ver: 2.0.p.0

Yii::import('bootstrap.widgets.TbNavbar');

class CpNavbar extends TbNavbar
{
 	//----------------------------------------------------------------------------
    public function init()
 	//----------------------------------------------------------------------------
	{
		$this->items = array(
			array(
				'class' => 'TbMenu',
				'items' => array(
					array('label' => 'Опции', 'items' => array(
						array('label' => 'Учетные записи', 'url' => array($r = '/users/user/admin'), 'visible' => Yii::app()->user->checkAccess($r)),
						array('label' => 'Резервные копии', 'url' => array($r = '/backuper/backup/admin'), 'visible' => Yii::app()->user->checkAccess($r)),					
						array('label' => 'Генератор кода', 'url' => array('/gii/default/index'), 'visible' => Yii::app()->user->checkAccess('gii') && Yii::app()->getModule('less')),
						array('label' => 'Скомпилировать LESS', 'url' => array('/less/less/compile'), 'visible' => Yii::app()->user->checkAccess('less') && Yii::app()->getModule('less')),
					)),
				),
			),

			array(
				'class' => 'TbMenu',		      
				'htmlOptions'=>array('class' => 'pull-right'),
				'items'=>array(
					array('label' => Yii::app()->user->name, 'url' => array($r = '/users/profile/view'), 'visible' => Yii::app()->user->checkAccess($r)), 
					array('label'=>'Войти', 'url'=>array('/users/login/login'), 'visible' => Yii::app()->user->isGuest),
					array('label'=>'Выйти', 'url'=>array('/users/login/logout'), 'visible' => !Yii::app()->user->isGuest)
					),
				),
		);

		parent::init();
    }
}

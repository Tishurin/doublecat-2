<?php 
	# ver: 2.0.0.1
?>

<!DOCTYPE html>
<html>
  <head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

    <title><?php echo CHtml::encode( (YII_DEBUG ? '~ ' : '') . $this->pageTitle  . ' | ' . Yii::app()->name); ?></title>

    <!-- Подключить Bootstrap -->
  	<?php Yii::app()->bootstrap->register() ?>

	<link rel="stylesheet" type="text/css" href="/css/cp.css" />
  </head>

  <body>

		<!-- Навигационная панель -->
		<?php $this->widget('application.views.cp.CpNavbar', array(
			'brandLabel' => 'DoubleCAT 2.0',
			'brandUrl' => Yii::app()->createUrl(Yii::app()->getModule('cp')->defaultController),
			'display' => null,
			'color' => TbHtml::NAVBAR_COLOR_INVERSE,
			'collapse' => true,
		)); ?>
		
		<div class='container-fluid'>
			<div class='row-fluid'>
				<div class='span2'>
					
					<!-- Левое меню -->
					<?php $this->widget('application.views.cp.CpLeftMenu', array(
						'type' => TbHtml::NAV_TYPE_LIST,
						'template' => '<div class="well" style="max-width: 340px; padding: 8px 0;">{menu}</div>',
						'htmlOptions' => array('class' => 'hidden-phone'),
					)); ?>

					<!-- Правое меню для гаджетов -->
					<?php $this->widget('TbMenu', array(
							'type' => TbHtml::NAV_TYPE_LIST,
							'htmlOptions' => array('class' => 'visible-phone'),							
						    'items' => $this->menu
					)); ?>
				
				</div>
				<div class='span8'>

					<!-- Хлебные крошки -->
					<?php if ($this->showBreadcrumbs): ?>
						<?php $this->widget('bootstrap.widgets.TbBreadcrumb', array(
							'homeLabel' => 'Главная',
							'homeUrl' => Yii::app()->createUrl(Yii::app()->getModule('cp')->defaultController),
						    'links' => $this->breadcrumbs + array($this->pageTitle)
						)); ?>
					<?php endif ?>

					<!-- Header -->
					<?php if ($this->showHeader): ?>
						<h1><?php echo Yii::app()->controller->pageTitle ?></h1>					
					<?php endif ?>

					<!-- Alert -->
					<?php $this->widget('bootstrap.widgets.TbAlert'); ?>

					<!-- Content -->
					<?php echo $content ?>	

				</div>
				<div class='span2 hidden-phone'>
					
						<?php $this->widget('TbMenu', array(
							'type' => TbHtml::NAV_TYPE_LIST,
							    'items' => $this->menu,
							    'template' => '<div class="well" style="max-width: 340px; padding: 8px 0;">{menu}</div>',
						)); ?>

				</div>
			</div>
		</div>

		<hr clear='all'>
		<div class='row-fluid'>
			<div class='span2'>&nbsp;</div>
			<div class='span8' style='text-align: center;'>  
				<footer>
					<p> 
						© <a href="http://dubus.ru">Dubus Group</a>, 2014.
					</p>
				</footer>
			</div>
		</div>

  </body>
</html>


<?php

# ver: 2.1.0
# Удалено автоматическое нахождение генератора хлебной крошки

class DController extends CController
{
	public $menu = array();
	public $breadcrumbs = array();
	public $titles = array();

	public $showHeader = true;
	public $showBreadcrumbs = true;
	public $defaultAction = 'index';
	public $errorView;

	private $_setPageTitle = false;

	//----------------------------------------------------------------------------
	public function setPageTitle($value)
	//----------------------------------------------------------------------------
	{
		$this->_setPageTitle = true;
		return parent::setPageTitle($value);
	}

	//----------------------------------------------------------------------------
	public function getPageTitle()
	//----------------------------------------------------------------------------
	{
		if(!$this->_setPageTitle && isset($this->titles[$this->action->id])) 
			$this->pageTitle = $this->titles[$this->action->id];

		return parent::getPageTitle();		
	}

	//----------------------------------------------------------------------------
	public function getTitle($action)
	//----------------------------------------------------------------------------
	{
		if (isset($this->titles[$action])) 
			return $this->titles[$action];
		
		else return ucfirst($action);	
	}

	//----------------------------------------------------------------------------
	public function addBreadcrumb($action, $url = '')
	//----------------------------------------------------------------------------
	// Добавить хлебную крошку для действия $action
	{
		$this->breadcrumbs[(isset($this->titles[$action]) ? $this->titles[$action] : $action)] = ($url == '' ? array($action) : $url);					
	}

	//----------------------------------------------------------------------------
	public function actionError()
	//----------------------------------------------------------------------------
	// Обработка ошибки 
	{
		$this->pageTitle = 'Произошла ошибка...';
		$this->showBreadcrumbs = false;

		if($error = Yii::app()->errorHandler->error)
		{
			if(Yii::app()->request->isAjaxRequest) echo $error['message'];
			else $this->render($this->errorView, $error);
		}
	}

	
	//----------------------------------------------------------------------------  
	public function filters()
	//----------------------------------------------------------------------------	
	{
		return array('rights');
	}

	//----------------------------------------------------------------------------
	public function filterRights($filterChain)
	//----------------------------------------------------------------------------  
	{
		if (Yii::app()->user->checkAccess($this->route)) $filterChain->run();
		else $this->accessDenied();
	}

	//----------------------------------------------------------------------------
	public function accessDenied($message = null)
	//----------------------------------------------------------------------------	
	{
		if($message === null)
			$message = 'Недостаточно прав для выполнения действия';

		$user = Yii::app()->getUser();
		if($user->isGuest === true) $user->loginRequired();
		else throw new CHttpException(404, $message);
	}

	//----------------------------------------------------------------------------  
	public function initPageStatePrevious($default = null)
	//----------------------------------------------------------------------------  
	{
		if ($default === null) $default = $this->createUrl($this->defaultAction); 
		$this->setPageState('previousUrl', Yii::app()->request->urlReferrer === null ? $default : Yii::app()->request->urlReferrer);
	}

	//----------------------------------------------------------------------------  
	public function getPageStatePrevious()
	//----------------------------------------------------------------------------  
	{
		return $this->getPageState('previousUrl', null);
	}

	//----------------------------------------------------------------------------  
	public function createRoute($route)
	//----------------------------------------------------------------------------  
	// Создать роут (для проверки прав доступа для пунктов меню)
	{
		if($route === '') $route=$this->getId().'/'.$this->getAction()->getId();
		elseif(strpos($route,'/') === false) $route = $this->getId() . '/' . $route;

		if($route[0] !== '/' && ($module = $this->getModule()) !== null)
		$route = $module->getId() . '/' . $route;	

		return $route;	
	}

	//----------------------------------------------------------------------------  
	public function checkAccess($route)
	//----------------------------------------------------------------------------  
	{
		return Yii::app()->user->checkAccess($this->createRoute($route));
	}

	//----------------------------------------------------------------------------  
	public function checkAccessOnce(&$route)
	//----------------------------------------------------------------------------  
	{
		if ($route === null) 
			throw new CException('Пустой роут при проверке прав');

		$r = $this->createRoute($route);
		$route = null;
		return Yii::app()->user->checkAccess($r);
	}

	//----------------------------------------------------------------------------  
	public function redirectPageStatePrevious()
	//----------------------------------------------------------------------------  
	{
		$this->redirect($this->pageStatePrevious);
	}

	//----------------------------------------------------------------------------  
	public function redirectBack()
	//----------------------------------------------------------------------------  
	{
		$this->redirect(Yii::app()->request->urlReferrer);
	}

	//----------------------------------------------------------------------------  
	public function redirect($url, $terminate = true, $statusCode = 302)
	//----------------------------------------------------------------------------  
	{
		parent::redirect($url, $terminate, $statusCode);
	}
}
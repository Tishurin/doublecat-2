<?php

# ver: 2.0.0

class DSortBehavior extends CActiveRecordBehavior
{
	public $parent; 				// Если используется конфигуратор
	public $sort = 'id_sort';    	// Поле сортировки (только по возрастанию)

	//----------------------------------------------------------------------------
	public function beforeSave($event)
	//----------------------------------------------------------------------------  
	// После вставки запись стала последней 
	{
		if ($this->owner->isNewRecord)
		{
			$dr = Yii::app()->db->createCommand()
			->select('MAX(' . $this->sort . ')')
			->from($this->owner->tableName());

			if ($this->parent !== null)  
				$dr->where($this->parent . ' = ' . $this->owner->attributes[$this->parent]);

			$intMaxOrder = $dr->queryScalar();
			$strOrder = $this->sort;

			$this->owner->$strOrder = $intMaxOrder + 2;
		}

	}

	//----------------------------------------------------------------------------
	public function incSort()
	//----------------------------------------------------------------------------	
	{
		$strOrder = $this->sort;
		$this->owner->$strOrder -= 3;
		$this->owner->save();
		$this->owner->_correctTable();
	}

	//----------------------------------------------------------------------------
	public function decSort()
	//----------------------------------------------------------------------------
	{
		$strOrder = $this->sort;
		$this->owner->$strOrder += 3;
		$this->owner->save();
		$this->owner->_correctTable();
	}

	//----------------------------------------------------------------------------
	public function _correctTable()
	//----------------------------------------------------------------------------	
	{
		Yii::app()->db->createCommand('set @a:=0')->execute();


		$strWhere = ($this->parent !== null) ? ' WHERE `' . $this->parent . '` = ' . $this->owner->attributes[$this->parent] : '';
		$cmd = Yii::app()->db->createCommand('UPDATE `' . $this->owner->tableName() . '` ' .
									  'SET ' . $this->sort . ' = (@a:=@a+2) ' . 
									  $strWhere . 
									  ' ORDER BY `' . $this->sort . '`');
		$cmd->execute();
	}
}
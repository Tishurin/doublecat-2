<?php

# ver: 2.0.1
# 2.0.1 добавлена функция getColumn

class DActiveRecord extends CActiveRecord
{
	private $_enum;

	//----------------------------------------------------------------------------
	public function getEnum()
	//----------------------------------------------------------------------------
	// Автоматически выдает текущие значения перечисляемых свойств
	{
		if ($this->_enum == null) {
			$this->_enum = array();

			foreach ($this->attributes as $k => $v) {
				if (property_exists($this, 'enum_' . $k)) {
					$prop = 'enum_' . $k;
					$arr = $this->$prop;
					$this->_enum[$k] = $arr[$v];
				}
				elseif (method_exists($this, 'getEnum_' . $k)) {
					$getter = 'getEnum_' . $k;
					$arr = $this->$getter();
					$this->_enum[$k] = $arr[$v];
				}
			}

		}

		return $this->_enum;
	}

	//----------------------------------------------------------------------------
	public function getColumn($column, $ucriteria = null, $index = 'id')
	//----------------------------------------------------------------------------
	// Запрос индексированных значений одного поля
	{
		$criteria = $this->getDbCriteria();

		if ($ucriteria) $criteria->mergeWith($ucriteria);

		$dr = $this->getCommandBuilder()->createFindCommand($this->getTableSchema(),$criteria,$this->getTableAlias())->query();

		$arrValues = array();
		foreach ($dr as $t) {
			$arrValues[$t[$index]] = $t[$column];
		}
        
		return $arrValues;
	}

	public static function model ($className=__CLASS__) {return parent::model($className);}
}


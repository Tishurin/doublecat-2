<?php

# ver: 2.0.0

class DSelectAction extends CAction
{
	public $modelClass;

	//----------------------------------------------------------------------------
    public function run($action = 'select', $d = 0)
	//----------------------------------------------------------------------------
    {
    	$function = 'action' . ucfirst($action); 

    	if (!method_exists($this, $function)) $this->controller->missingAction();
    	else $this->$function();
    }

	//----------------------------------------------------------------------------
	public function actionSelect()
	//----------------------------------------------------------------------------
	{
		$modModel = new $this->modelClass;
		if (isset($_POST['id'])) {
			foreach ($_POST['id'] as $id) 
				$modModel->selectByPk($id);	
		} else {
			Yii::app()->user->setFlash('warning', 'Для того чтобы выделить товары, отметьте их галочками слева');
		}

		$this->controller->redirectBack();
	}

	//----------------------------------------------------------------------------
	public function actionUnselect()
	//----------------------------------------------------------------------------
	{
		$modModel = new $this->modelClass;
		$modModel->unselectAll();
		$this->controller->redirectBack();
	}

	//----------------------------------------------------------------------------
	public function actionMove()
	//----------------------------------------------------------------------------
	{
		$objModel = new $this->modelClass;

		try {
			foreach ($objModel->selectedPk as $id) 
			{
				$modModel = $objModel->findByPk($id);
				$modModel->asa('tree')->move((int)$_GET['id']);
				$modModel->save();				
			}

		} catch (CException $e) {
			Yii::app()->user->setFlash('error', $e->getMessage());

		}

		$objModel->unselectAll();
		$this->controller->redirectBack();
	}





}
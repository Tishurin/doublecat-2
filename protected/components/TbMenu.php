<?php

# ver: 2.0.0

Yii::import('bootstrap.widgets.TbNav');

class TbMenu extends TbNav
{
	public $template = '{menu}';
	public $checkAccess;

 	//----------------------------------------------------------------------------
	public function getIsEmpty()
 	//----------------------------------------------------------------------------
	{
		if (empty($this->items)) return true;

		foreach ($this->items as $sttItem) 
			if (isset($sttItem['url']) || isset($sttItem['items']))
				return false;



		return true;
	}

 	//----------------------------------------------------------------------------
    public function run()
 	//----------------------------------------------------------------------------
	{
        if (!$this->isEmpty)
            echo strtr($this->template, array('{menu}' => TbHtml::nav($this->type, $this->items, $this->htmlOptions)));
    }
}

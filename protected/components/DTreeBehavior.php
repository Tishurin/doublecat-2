<?php

# ver: 1.2.1
# 1.2.1 - схема запоминается в глобальных настройках приложения

class DTreeBehavior extends CActiveRecordBehavior
{
	public $parent = 'id_parent';
	private $_children;

	public $dependency;

	//----------------------------------------------------------------------------
	public function beforeDelete($event)
	//----------------------------------------------------------------------------  
	// Удалить всех потомков
	{
		foreach ($this->getChildren() as $modChild) {
			$modChild->delete();
		}
	}

	//----------------------------------------------------------------------------
	public function getSchema()
	//----------------------------------------------------------------------------
	{
		$p = Yii::app()->params;

		if (!Yii::app()->params[get_class($this->owner)]) {
			$dp = Yii::app()->db->createCommand()->
				select(array('id', $this->parent))->
				from($this->owner->tableName())->query();
			
			$arrRet = array();
			foreach($dp as $t)
				$arrRet[$t['id']] = $t[$this->parent];

			Yii::app()->params[get_class($this->owner)] = $arrRet;
		}

		return Yii::app()->params[get_class($this->owner)];
	}

	//----------------------------------------------------------------------------
	public function getChildren()
	//----------------------------------------------------------------------------
	// Прямые потомки
	{		
		if($this->_children == null) 
			$this->_children = $this->owner->findAllByAttributes(array($this->parent => $this->owner->id));

		return $this->_children;
	}

	//----------------------------------------------------------------------------
	public function getParentIds()
	//----------------------------------------------------------------------------
	{
		$arrParentIds = array();

		$id = $this->owner->id;
		while ($id = $this->schema[$id])
			$arrParentIds[] = $id;

		return array_reverse($arrParentIds);
	}

	//----------------------------------------------------------------------------
	public function getParents()
	//----------------------------------------------------------------------------
	// Выдает всех предков
	{
		$arrParentIds = $this->getParentIds();
	
		// Запросить родителей по ключам
		$arrPreParents = $this->owner->findAllByPk($arrParentIds, array('index' => 'id'));
		

		// Отсортировать родителей в правильном порядке
		$arrRet = array();
		foreach ($arrParentIds as $v) 
			$arrRet[] = $arrPreParents[$v];

		return $arrRet;
	}

	//----------------------------------------------------------------------------
	public function move($id)
	//----------------------------------------------------------------------------
	{
		$modParent = $this->owner->findByPk($id);

		foreach ($modParent->parentIds as $parent_id) 
			if (in_array($this->owner->id, $modParent->parentIds) || $this->owner->id == $id)
				throw new CException('Нельзя перемещать в собственный подраздел!');

		$this->owner[$this->parent] = $id;	
	}

}


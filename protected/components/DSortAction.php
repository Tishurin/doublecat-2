<?php

class DSortAction extends CAction
{
	//----------------------------------------------------------------------------
    public function run($id, $d = 0)
	//----------------------------------------------------------------------------
    {
    	$modModel = $this->controller->loadModel($id);
    	if ($d) $modModel->asa('sort')->decSort();
    	else $modModel->asa('sort')->incSort();
    	$this->controller->redirectBack();
    }
}
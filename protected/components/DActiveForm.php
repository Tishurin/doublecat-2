<?php

# ver: 2.0.1
# 2.0.1 - Добавлена дата

class DActiveForm extends CActiveForm
{
	//----------------------------------------------------------------------------
	public function fileField($model,$attribute,$htmlOptions=array())
	//----------------------------------------------------------------------------
	{
	    return DFile::activeFileField($model, $attribute, $htmlOptions);
	}

	//----------------------------------------------------------------------------
	public function imageField($model,$attribute,$htmlOptions=array())
	//----------------------------------------------------------------------------
	{
	    return DFile::activeImageField($model, $attribute, $htmlOptions);
	}


	//----------------------------------------------------------------------------
	public function dateField($model, $attribute, $htmlOptions = array())
	//----------------------------------------------------------------------------
	{
		$this->widget('zii.widgets.jui.CJuiDatePicker', array(
									//'model' => $model,
									//'attribute' => $attribute,
									'name' => CHtml::resolveName($model, $attribute),
									'value' => ($model->$attribute != null && $model->$attribute != '' && $model->$attribute != 0 ? date('d.m.Y', $model->$attribute) : ''),
									'options' => array(
										'dateFormat' => 'dd.mm.yy'
										),
									'htmlOptions' => $htmlOptions));
	}

	//----------------------------------------------------------------------------
	public function htmlArea($model, $attribute, $htmlOptions = array())
	//----------------------------------------------------------------------------
	// Можно через htmlOptions вызвать обработчик загруженного рисунка:
	// 'options' => array('imageUploadCallback' => 'function(image, json){image.attr("data-id", json.fileId);}')
	{
		$imageUploadCallback = isset($htmlOptions['imageUploadCallback']) ? new CJavaScriptExpression($htmlOptions['imageUploadCallback']) : false;
		unset($htmlOptions['imageUploadCallback']);

		$options = isset($htmlOptions['options']) ? $htmlOptions['options'] : array();

		unset($htmlOptions['options']);

		if (isset($options['imageUploadCallback'])) 
			$options['imageUploadCallback'] = new CJavaScriptExpression($options['imageUploadCallback']);

		$this->widget('ext.imperavi.ImperaviRedactorWidget', array(
			'model' => $model,
			'attribute' => $attribute,
			'options' => CMap::mergeArray(
				array(
				'lang' => 'ru',
				'toolbar' => true,
				'iframe' => true,
				'imageUpload' => Yii::app()->controller->createUrl('upload'),
				), 
				$options
			),
			'htmlOptions' => $htmlOptions
		));
	}
}
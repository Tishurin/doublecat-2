<?php

# ver: 2.0.0

class DFormatter extends CFormatter
{
	public $dateFormat = 'd.m.Y';
	public $datetimeFormat = 'd.m.Y, H:i';
	public $timeFormat = 'H:i';
	public $numberFormat = array('decimals' => 0, 'decimalSeparator' => ',', 'thousandSeparator' => ' ');  
	public $booleanFormat = array('Нет', 'Да');
	
	//----------------------------------------------------------------------------
	public function getEnumYears()
	//----------------------------------------------------------------------------
	{
		$arrRet = array();
		for ($i = (date('Y') + 1); $i > 2000; $i--) $arrRet[$i] = $i;
		return $arrRet;
	}

	//----------------------------------------------------------------------------
	public function formatDate($value)
	//----------------------------------------------------------------------------	
	{
		if ($value == 0) return '';
		return date($this->dateFormat, $value);
	}


}
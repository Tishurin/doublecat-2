<?php


return CMap::mergeArray(
	require(dirname(__FILE__) . '/config_main.php'),
	array(


	'modules'=>array(
		'less' => array(
				'class' => 'system.vendors.less.LessModule',
				'files' => array(
					array(
						'less' => dirname(__FILE__) . '/../extensions/bootstrap/assets/less/bootstrap.less',
						'css' => dirname(__FILE__) . '/../extensions/bootstrap/assets/css/bootstrap.css',
						),
					
					array(
						'less' => dirname(__FILE__) . '/../extensions/bootstrap/assets/less/bootstrap.less',
						'css' => dirname(__FILE__) . '/../extensions/bootstrap/assets/css/bootstrap.min.css',
						'formatter' => 'compressed',
						)
				),
			),

		'gii'=>array(
			'class' => 'system.gii.GiiModule', // /cp/gii/default/login
			'password' => '123',			
			'ipFilters' => array('127.0.0.1','::1'),
			'generatorPaths'=>array('ext.gii'),			

		),
	),

	'components' => array(

		'db'=>array(
			'database' => 'dw_doublecat_2_dlp',
			'username' => 'root',
			'password' => '123',
			'enableProfiling' => true,
			'enableParamLogging' => true
			),

		'log' => array(
			'routes'=>array(
				array(
					'class'=>'system.vendors.db_profiler.DbProfileLogRoute',
					'countLimit' => 1, 
					'slowQueryMin' => 0.01,
					'enabled' => true,
				),								
		)),

	),

));

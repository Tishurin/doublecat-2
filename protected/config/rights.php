<?php

return array(
	// Гость может только авторизоваться
	'guest' => array(
		'users' => array('login'),
		'cp' => array('cp' => array('error')),
		
		'test' => array(
				'test' => array(
					'create',	
					)
			),
		'site',
	),

	'administrator' => array(
		'cp',
		'users' => array('profile'),
	),

);
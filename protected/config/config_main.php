<?php

define('THUMB_MODE_CROP', 0);
define('THUMB_MODE_FILL', 1);
define('THUMB_MODE_GRIP', 2);

require_once dirname(__FILE__) . '/../components/DBasicApi.php';

return array(

	'basePath'=>dirname(__FILE__).DIRECTORY_SEPARATOR.'..',

	'name'=>'My Web Application',
	'language' => 'ru',

	'defaultController' => 'site/site', // Или cp/cp, если модуля Сайт нет

	'preload'=>array('log'),

    'aliases' => array('bootstrap' => dirname(__FILE__) . '/../extensions/bootstrap'),

	'import'=>array(
		'application.models.*',
		'application.components.*',
		'application.modules.cp.components.*',
		
		'application.modules.users.components.*',
		'application.modules.users.models.*',

		'application.modules.files.components.*',
		'application.modules.files.models.*',

		'bootstrap.helpers.TbHtml',
	),

	// Модули
	'modules'=>array(
		'cp' => array(),
		'site',
		'users',
		'test',
		'settings',
		'files' => array(
			'debug' => false,
			'noimageUrl' => '/images/noimage/179x179.png',
			'thumbs' => array(
				'mic' => array('width' => 36, 'height' => 36, 'noimageUrl' => '/images/noimage/36x36.png'),
				'min' => array('width' => 139, 'height' => 122, 'mode' => THUMB_MODE_FILL, 'noimageUrl' => '/images/noimage/139x122.png'),
				'html' => array('width' => 600, 'mode' => THUMB_MODE_GRIP, 'noimageUrl' => '/images/noimage/179x179.png'),
			),	
		),




		'backuper' => array('serverName' => 'LCH'),
	),

	// Компоненты
	'components'=>array(
		'settings' =>  array(
			'class' => 'settings.components.DSettingsComponent',
			'items' => array(
					'email' => 'Почта',
					'description' => array('label' => 'Описание', 'type' => 'textArea', 'style' => 'height: 100px;'),
					'<hr>',
					'dropdown' => array('label' => 'Выпадашка', 'type' => 'dropDownList', 'items' => array('asdf', 'fdsa')),
					'test' => 'Проверочная',
				),
			),

		'format' => array('class' => 'DFormatter'),

		'user'=>array(
			'allowAutoLogin'=>true,
			'class' => 'DWebUser',
			'loginUrl' => array('/users/login/login')),

		'authManager' => array(
			'class' => 'DPhpAuthManager',
			'defaultRoles' => array('guest')),
		
		'bootstrap' => array(
			'class' => 'bootstrap.components.TbApi',   
			'forceCopyAssets' => false,
		),

		'urlManager'=>array(
			'urlFormat' => 'path',
			'showScriptName' => false,
			'useStrictParsing' => false,
			'rules'=>array(
				'' => '',

				// Шаблоны контрольной панели (лучше не трогать, на них завязана обработка ошибок)
				'login' => 'users/login/login',
				'cp' => 'cp/cp/index',
				'cp/<action:\w+>' => 'cp/cp/<action>',
				'cp/<controller:\w+>/<action:\w+>' => 'cp/<controller>/<action>',
				'cp/<module:(?!site)\w+>/<controller:\w+>/<action:\w+>' => '<module>/<controller>/<action>',

				
				// Шаблоны сайта
				'<controller:\w+>' => 'site/<controller>', // Действия по умолчанию, для создания УРЛа роут писать без действия!
				'<controller:\w+>/<action:\w+>' => 'site/<controller>/<action>',
			),
		),

		'db'=>array(
			'class' => 'DDbConnection',
			'emulatePrepare' => false,
			'charset' => 'utf8',
			'enableProfiling' => true,
			'enableParamLogging' => true

			),

		'errorHandler'=>array(
			'errorAction' => 'site/site/error',	// Или 'cp/cp/error'
		),

		'cache'=>array(
            'class'=>'system.caching.CFileCache',
        ),

		'log' => array(
			'class'=>'CLogRouter',
		),
	),
);
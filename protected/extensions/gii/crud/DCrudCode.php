<?php

# ver: 2.0.1
# Генерация дерева

Yii::import('system.gii.generators.crud.CrudCode');

class DCrudCode extends CrudCode
{
	public $baseControllerClass = 'DcController';

	public $treeNavigation = 0;

	//----------------------------------------------------------------------------
	public function rules()
	//----------------------------------------------------------------------------
	{
		return array_merge(parent::rules(), array(
			array('treeNavigation', 'numerical', 'integerOnly' => true),
		));
	}

	//----------------------------------------------------------------------------
	public function attributeLabels()
	//----------------------------------------------------------------------------
	{
		return array_merge(parent::attributeLabels(), array(
			'treeNavigation' => 'Создать навигацию по дереву',
		));
	}

	//----------------------------------------------------------------------------
	public function generateInputLabel($modelClass,$column)
	//----------------------------------------------------------------------------
	{
		return "CHtml::activeLabelEx(\$mod" . $this->modelClass . ", '{$column->name}')";
	}

	//----------------------------------------------------------------------------
	public function generateInputField($modelClass,$column)
	//----------------------------------------------------------------------------
	{
		if($column->type==='boolean')
			return "CHtml::activeCheckBox(\$mod" . $this->modelClass . ", '{$column->name}')";
		else if(stripos($column->dbType,'text')!==false)
			return "CHtml::activeTextArea(\$mod" . $this->modelClass . ", '{$column->name}', array('rows'=>6, 'cols'=>50))";
		else
		{
			if(preg_match('/^(password|pass|passwd|passcode)$/i',$column->name))
				$inputField='activePasswordField';
			else
				$inputField='activeTextField';

			if($column->type!=='string' || $column->size===null)
				return "CHtml::{$inputField}(\$mod" . $this->modelClass . ", '{$column->name}')";
			else
			{
				if(($size=$maxLength=$column->size)>60)
					$size=60;
				return "CHtml::{$inputField}(\$mod" . $this->modelClass . ", '{$column->name}',array('size'=>$size,'maxlength'=>$maxLength))";
			}
		}
	}

	//----------------------------------------------------------------------------
	public function generateActiveLabel($modelClass,$column)
	//----------------------------------------------------------------------------
	{
		return "\$form->labelEx(\$mod" . $this->modelClass . ", '{$column->name}', array('class' => 'control-label'))";
	}

	//----------------------------------------------------------------------------
	public function generateActiveField($modelClass,$column)
	//----------------------------------------------------------------------------
	{
		if($column->type==='boolean')
			return "\$form->checkBox(\$mod" . $this->modelClass . ", '{$column->name}')";
		else if(stripos($column->dbType,'text')!==false)
			return "\$form->textArea(\$mod" . $this->modelClass . ", '{$column->name}', array('class' => 'span12'))";
		else
		{
			if(preg_match('/^(password|pass|passwd|passcode)$/i',$column->name))
				$inputField='passwordField';
			else
				$inputField='textField';

			if($column->type!=='string' || $column->size===null)
				return "\$form->{$inputField}(\$mod" . $this->modelClass . ", '{$column->name}', array('class' => 'span12'))";
			else
			{
				if(($size=$maxLength=$column->size)>60)
					$size=60;
				return "\$form->{$inputField}(\$mod" . $this->modelClass . ", '{$column->name}', array('class' => 'span12'))";
			}
		}
	}

}
<?php

# ver: 2.0.0.1
# Убрал под комментарий фильтры

/**
 * The following variables are available in this template:
 * - $this: the CrudCode object
 */
?>

<?php echo "<?php \$form=\$this->beginWidget('CActiveForm', array(
	'action' => Yii::app()->createUrl(\$this->route),
	'method' => 'get',
	'htmlOptions' => array('style' => 'margin-bottom: 0;'),
)); ?>\n"; ?>
	<div class='well well-small' style='padding-right: 130px; margin-bottom: 15px;'>
		<?php echo "<?php echo TbHtml::submitButton('Искать', array('color' => TbHtml::BUTTON_COLOR_PRIMARY, 'type' => TbHtml::BUTTON_TYPE_SUBMIT, 'icon' => 'icon-search icon-white', 'style' => 'float: right; text-align: left; margin-right: -120px; width: 100px;')); ?> \n"?>
		<?php echo "<?php echo \$form->textField(\$mod{$this->modelClass}, 'request', array('style' => 'margin-bottom: 0; width: 100%;', 'placeholder' => 'Введите поисковое слово')); ?> \n" ?>
	</div>
<?php echo "<?php \$this->endWidget(); ?>\n"; ?>

<?php if ($this->treeNavigation): ?>
<?php echo "<?php echo CHtml::beginForm(array('select'), 'POST', array('class' => 'tree-select-form')) ?>\n" ?>
<?php endif ?>

<?php echo "<?php"; ?> $this->widget('bootstrap.widgets.TbGridView', array(
	'id' => '<?php echo $this->class2id($this->modelClass); ?>-grid',
	'dataProvider' => $mod<?php echo $this->modelClass; ?>->search(),
	'type'=>'striped bordered',
	'template' => '{items}{pager}',
	'selectableRows' => 0,
	'emptyText' => 'Ничего не нашлось...',
	//'filter' => $mod<?php echo $this->modelClass; ?>,
	'ajaxUpdate' => false,
	'columns'=>array(
<?php if ($this->treeNavigation): ?>
		array(
			'class'=>'CCheckBoxColumn',
			'selectableRows' => 2,
			'checkBoxHtmlOptions' => array('name' => 'id[]'),
			'checked' => '$data->isSelected',
			'disabled' => '$data->hasSelection',
			'value' => '$data->id',
		),
		array('class' => 'CLinkColumn', 'labelExpression' => '$data->getTitle()', 'urlExpression' => 'array("admin", "id" => $data->id)', 'htmlOptions' => array()),
<?php endif ?><?php
$count=0;
foreach($this->tableSchema->columns as $column)
{
	if(++$count==7)
		echo "\t\t/*\n";
	echo "\t\tarray('name' => '".$column->name."', 'htmlOptions' => array()),\n";
}
if($count>=7)
	echo "\t\t*/\n";
?>

<?php $hasSort = isset($this->tableSchema->columns['id_sort']) ?>
		array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
			'template' => '<?php if ($hasSort): ?>{moveUp} {moveDown} <?php endif ?>{view} {update} {delete}',
			'htmlOptions' => array('style' => 'width: <?php echo $hasSort ? 90 : 50 ?>px;'),
			'buttons' => array(
<?php if ($hasSort): ?>
				'moveUp' => array(
					'icon' => 'arrow-up',
					'url' => 'Yii::app()->controller->createUrl("sort", array("id" => $data->id))',
					'label' => 'Передвинуть вверх',
					),
				'moveDown' => array(
					'icon' => 'arrow-down',
					'url' => 'Yii::app()->controller->createUrl("sort", array("sort", "id" => $data->id, "d" => 1))',
					'label' => 'Передвинуть вниз',					
					),	
<?php endif ?>
			),
		),
	),
)); ?>

<?php if ($this->treeNavigation): ?><?php echo "<?php echo CHtml::endForm() ?>\n" ?><?php endif ?>


<?php
# ver: 2.0.0

/**
 * The following variables are available in this template:
 * - $this: the CrudCode object
 */
?>

<?php echo "<?php \$form=\$this->beginWidget('CActiveForm', array(
	'action' => Yii::app()->createUrl(\$this->route),
	'method' => 'get',
	'htmlOptions' => array('style' => 'margin-bottom: 0;'),
)); ?>\n"; ?>
	<div class='well well-small' style='padding-right: 130px; margin-bottom: 0;'>
		<?php echo "<?php echo TbHtml::submitButton('Искать', array('color' => TbHtml::BUTTON_COLOR_PRIMARY, 'type' => TbHtml::BUTTON_TYPE_SUBMIT, 'icon' => 'icon-search icon-white', 'style' => 'float: right; text-align: left; margin-right: -120px; width: 100px;')); ?> \n"?>
		<?php echo "<?php echo \$form->textField(\$mod{$this->modelClass}, 'request', array('style' => 'margin-bottom: 0; width: 100%;')); ?> \n" ?>
	</div>
<?php echo "<?php \$this->endWidget(); ?>\n"; ?>


<?php echo "<?php"; ?> $this->widget('bootstrap.widgets.TbListView', array(
	'dataProvider' => $mod<?php echo $this->modelClass; ?>->search(),
  'template' => '{pager}{items}{pager}',
	'itemView' => 'listItem',
)); ?>

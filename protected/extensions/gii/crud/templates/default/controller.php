<?php
# ver: 2.0.1
# Удален список, добавлено дерево

/**
 * This is the template for generating a controller class file for CRUD feature.
 * The following variables are available in this template:
 * - $this: the CrudCode object
 */
?>
<?php echo "<?php\n"; ?>
<?php $hasSort = isset($this->tableSchema->columns['id_sort']) ?>

class <?php echo $this->controllerClass; ?> extends <?php echo $this->baseControllerClass."\n"; ?>
{
	public $defaultAction = 'admin';

<?php if ($this->treeNavigation): ?>
	// Навигация по дереву!
<?php endif ?>

	public $titles = array(
		'admin' => 'Администрирование',
		'update' => 'Редактировать',
		'create' => 'Добавить',
		'view' => 'Просмотр',	// Меняется в loadModel
		'list' => 'Список',	
		);

<?php if ($hasSort || $this->treeNavigation): ?>
	//----------------------------------------------------------------------------
	public function actions()
	//----------------------------------------------------------------------------
	{
		return array(
			<?php if ($hasSort): ?>'sort' => array(
				'class' => 'DSortAction',
			),<?php endif ?>
			<?php if ($this->treeNavigation): ?>'select' => array(
				'class' => 'DSelectAction',
				'modelClass' => '<?php echo $this->modelClass; ?>',
			),<?php endif ?>

		);
	}	
<?php endif ?>


	//----------------------------------------------------------------------------
	public function actionAdmin(<?php if ($this->treeNavigation): ?>$id = 0<?php endif ?>)
	//----------------------------------------------------------------------------
	{
		$mod<?php echo $this->modelClass; ?> = new <?php echo $this->modelClass; ?>('search');
		$mod<?php echo $this->modelClass; ?>->unsetAttributes();
		
		<?php if ($this->treeNavigation): ?>
		// Навигация по дереву
		$mod<?php echo $this->modelClass; ?>->id_parent = $id;
		$modParent = <?php echo $this->modelClass; ?>::model()->findByPk($id);
		$this->addAdminBreadcrumb($modParent);
		if ($modParent) $this->pageTitle = $modParent->getTitle();
		<?php endif ?>

		if(isset($_GET['<?php echo $this->modelClass; ?>'])) $mod<?php echo $this->modelClass; ?>->attributes = $_GET['<?php echo $this->modelClass; ?>'];

		$this->menu = array(                          
			array('label' => 'Опции'), 

			array('label' => $this->getTitle('create'), 
					'icon' => 'plus',
					'url' => array($r = 'create'<?php if ($this->treeNavigation): ?>, 'id_parent' => $id<?php endif ?>), 
					'visible' => $this->checkAccessOnce($r),
			),<?php if ($this->treeNavigation): ?>
			array('label' => $this->getTitle('update'), 
					'icon' => 'pencil',
					'url' => array($r = 'update', 'id' => $id), 
					'visible' => $id <> 0 && $this->checkAccessOnce($r),
			),
			array('label' => 'Вырезать', 
						'icon' => 'file',
						'url' => '#',
						'linkOptions' => array('onclick' => 'jQuery(".tree-select-form").submit(); return false;'),
						'visible' => !$mod<?php echo $this->modelClass; ?>->hasSelection && $this->checkAccessOnce($r = 'select')),

			array('label' => 'Вставить', 
						'icon' => 'file',
						'url' => array($r = 'select', 'action' => 'move', 'id' => $id), 
						'linkOptions' => array('confirm' => 'Точно?'),
						'visible' => $mod<?php echo $this->modelClass; ?>->hasSelection && Yii::app()->user->checkAccess($r)),

			array('label' => 'Отменить', 
						'icon' => 'file',
						'url' => array($r = 'select', 'action' => 'unselect') , 
						'visible' => $mod<?php echo $this->modelClass; ?>->hasSelection && Yii::app()->user->checkAccess($r)),
			



		<?php endif ?>
		);


		$this->render('admin', array('mod<?php echo $this->modelClass; ?>' => $mod<?php echo $this->modelClass; ?>));
	}

	//----------------------------------------------------------------------------
	public function actionView($id)
	//----------------------------------------------------------------------------
	{
		$mod<?php echo $this->modelClass ?> = $this->loadModel($id);
		
		$this->menu = array(
			array('label' => 'Опции'), 
			
			array('label'=>'Редактировать', 
						'icon' => 'pencil',
						'url' => array($r = 'update', 'id' => $mod<?php echo $this->modelClass ?>-><?php echo $this->tableSchema->primaryKey; ?>), 
						'visible' => $this->checkAccessOnce($r),
			),
			
			array('label'=>'Удалить', 
						'icon' => 'trash',
						'url' => '#', 
						'linkOptions'=>array('submit'=>array($r = 'delete', 'id' => $mod<?php echo $this->modelClass ?>-><?php echo $this->tableSchema->primaryKey; ?>), 'confirm'=>'Точно?'), 
						'visible' => $this->checkAccessOnce($r),
			),
		);

		$this->addAdminBreadcrumb($mod<?php echo $this->modelClass ?>);
		
		$this->render('view', array('mod<?php echo $this->modelClass ?>' => $mod<?php echo $this->modelClass ?>));
	}

	//----------------------------------------------------------------------------
	public function actionCreate(<?php if ($this->treeNavigation): ?>$id_parent = 0<?php endif ?>)
	//----------------------------------------------------------------------------
	{
		$mod<?php echo $this->modelClass ?> = new <?php echo $this->modelClass; ?>;

		<?php if ($this->treeNavigation): ?>// Навигация по дереву
		$mod<?php echo $this->modelClass ?>->id_parent = $id_parent;
		$modParent = <?php echo $this->modelClass; ?>::model()->findByPk($id_parent);
		$this->addAdminBreadcrumb($modParent, true);
		<?php endif ?>

		if(isset($_POST['<?php echo $this->modelClass; ?>']))
		{
			$mod<?php echo $this->modelClass ?>->attributes = $_POST['<?php echo $this->modelClass; ?>'];
			if($mod<?php echo $this->modelClass ?>->save())
				$this->redirectPageStatePrevious();
		}
		else $this->initPageStatePrevious();

		$this->addAdminBreadcrumb();

		$this->render('create', array('mod<?php echo $this->modelClass ?>' => $mod<?php echo $this->modelClass ?>));
	}

	//----------------------------------------------------------------------------
	public function actionUpdate($id)
	//----------------------------------------------------------------------------
	{
		$mod<?php echo $this->modelClass ?> = $this->loadModel($id);

		if(isset($_POST['<?php echo $this->modelClass; ?>']))
		{
			$mod<?php echo $this->modelClass ?>->attributes = $_POST['<?php echo $this->modelClass; ?>'];
			if($mod<?php echo $this->modelClass ?>->save())
				$this->redirectPageStatePrevious();
		}
		else $this->initPageStatePrevious(array('view', 'id' => $mod<?php echo $this->modelClass ?>-><?php echo $this->tableSchema->primaryKey; ?>)); // Переопределите если нужно

		$this->addViewBreadcrumb($mod<?php echo $this->modelClass ?>);

		$this->render('update', array('mod<?php echo $this->modelClass ?>' => $mod<?php echo $this->modelClass ?>));
	}

	//----------------------------------------------------------------------------
	public function actionDelete($id)
	//----------------------------------------------------------------------------  
	{
		if(Yii::app()->request->isPostRequest)
		{
			// we only allow deletion via POST request
			$this->loadModel($id)->delete();

			// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
			if(!isset($_GET['ajax']))
				$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
		}
		else
			throw new CHttpException(400,'Ошибка запроса.');
	}

	//****************************************************************************
	// Хлебные крошки
	//****************************************************************************

	//----------------------------------------------------------------------------
	public function addAdminBreadcrumb($modCurrent = null, $withCurrent = false)
	//----------------------------------------------------------------------------
	{
		<?php if (!$this->treeNavigation): ?>$this->addBreadcrumb('admin');<?php else: ?>if ($modCurrent) {
			$this->addBreadcrumb('admin');
			foreach ($modCurrent->parents as $modParent) 
				$this->addBreadcrumb($modParent->name, array('admin', 'id' => $modParent->id));
			if ($withCurrent) $this->addBreadcrumb($modCurrent->name, array('admin', 'id' => $modCurrent->id));
		}<?php endif ?>	
	}

	//----------------------------------------------------------------------------
	public function addViewBreadcrumb($modCurrent)
	//----------------------------------------------------------------------------
	{
		$this->addAdminBreadcrumb($modCurrent);
		$this->addBreadcrumb('view', array('view', 'id' => $modCurrent->id));
	}

	//----------------------------------------------------------------------------
	public function loadModel($id)
	//----------------------------------------------------------------------------
	{
		$mod<?php echo $this->modelClass; ?> = <?php echo $this->modelClass; ?>::model()->findByPk($id);
		if($mod<?php echo $this->modelClass; ?> === null)
			throw new CHttpException(404,'Такой страницы нет!');

		$this->titles['view'] = $mod<?php echo $this->modelClass; ?>->getTitle(); 	
		return $mod<?php echo $this->modelClass; ?>;
	}
}
